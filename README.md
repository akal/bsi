# Introduction #
**BSI** is Blender Streamlined Interaction project. The intent of the project is a nod to Blender and wink to Softimage. 

The project aims to be bring many of the features found in Softimage to Blender 3D. The features to be brought over are keyboard shortcuts, interaction, manipulation and a familiar SI gui. There are certain feature that won't be brought over due to internal architecture of Blender 3D. Such as new anchored windows,  persistent floating dialog boxes, various mechanics of the ui system, 

# Install #
To run this add-on. The user will need to manually copy the contents to appropriate locations. The addon goes in to the addon directory. The ~/startup folders content to blender ~/statup folder.

## Details ##
**/somefolder/bsi/** <-- from here
**/home/username/bin/blender/2.75/scripts/addons/bsi/** <-- to here

**/somefolder/bsi/startup/** <-- from here
**/home/username/bin/blender/2.75/scripts/startup/** <-- to here
