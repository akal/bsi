import bpy

#class VIEW3D_MT_uv_map(Menu):
#    bl_label = "UV Mapping"

#    def draw(self, context):
#        layout = self.layout

#        layout.operator("uv.unwrap")

#        layout.operator_context = 'INVOKE_DEFAULT'
#        layout.operator("uv.smart_project")
#        layout.operator("uv.lightmap_pack")
#        layout.operator("uv.follow_active_quads")

#        layout.separator()

#        layout.operator_context = 'EXEC_REGION_WIN'
#        layout.operator("uv.cube_project")
#        layout.operator("uv.cylinder_project")
#        layout.operator("uv.sphere_project")

#        layout.separator()

#        layout.operator_context = 'INVOKE_REGION_WIN'
#        layout.operator("uv.project_from_view").scale_to_bounds = False
#        layout.operator("uv.project_from_view", text="Project from View (Bounds)").scale_to_bounds = True

#        layout.separator()

#        layout.operator("uv.reset")

class BSI_UV_tools(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Tools"
    bl_idname = "IMAGE_PT_tools_transform_uvs"
    bl_space_type = 'IMAGE_EDITOR'
    bl_region_type = 'TOOLS'
    bl_category = "Tools"

    def draw(self, context):
        layout = self.layout
        obj_data = context.object.data       
        row = layout.row()
        col = row.column()
        col.template_list("MESH_UL_uvmaps_vcols", "uvmaps", obj_data, 
                    "uv_textures", obj_data.uv_textures, "active_index", rows=1)
        col = row.column(align=True)
        col.operator("mesh.uv_texture_add", icon='ZOOMIN', text="")
        col.operator("mesh.uv_texture_remove", icon='ZOOMOUT', text="")
        col.operator("bsi.uv_texture_from_selection", icon='IMAGE_DATA', text="")
        
        box =layout.box()
        col = box.column(align=True)
        row = col.row(align=True)
        #row.operator("mesh.uv_texture_add", text="Add UV Channel", icon='ZOOMIN')        
        row = col.row(align=True)
        row.operator("uv.smart_project")
        row.operator("uv.lightmap_pack")
        row = col.row(align=True)
        row.operator("uv.follow_active_quads", text="Active Quads")

        box =layout.box()
        col = box.column(align=True)
        row = col.row(align=True)
        row.operator("uv.cube_project", text="Cubic")

        row = col.row(align=True)
        cylinder_uv = row.operator("uv.cylinder_project", text="Cylinder ZX")
        cylinder_uv.direction = 'VIEW_ON_EQUATOR'
        cylinder_uv.align = 'POLAR_ZX'
        cylinder_uv.clip_to_bounds = True
        cylinder_uv.scale_to_bounds = True
        cylinder_uv = row.operator("uv.cylinder_project", text="Cylinder ZY")
        cylinder_uv.direction = 'ALIGN_TO_OBJECT'
        cylinder_uv.align = 'POLAR_ZY'
        cylinder_uv.scale_to_bounds = True

        row = col.row(align=True)
        sphere_uv = row.operator("uv.sphere_project", text="Sphere ZX")
        sphere_uv.direction = 'VIEW_ON_EQUATOR'
        sphere_uv.align = 'POLAR_ZX'
        sphere_uv = row.operator("uv.sphere_project", text="Sphere ZY")
        sphere_uv.direction = 'ALIGN_TO_OBJECT'
        sphere_uv.align = 'POLAR_ZY'

        box =layout.box()
        col = box.column(align=True)
        row = col.row(align=True)
        unwrap = row.operator("uv.unwrap")
        unwrap.method = 'ANGLE_BASED'
        unwrap.fill_holes = True
        unwrap.correct_aspect = True
        unwrap.use_subsurf_data = False
        unwrap.use_subsurf_data = True
        unwrap = row.operator("uv.seams_from_islands", text="Island Seams")
        
        row = col.row(align=True)
        unwrap = row.operator("uv.mark_seam").clear = False
        unwrap = row.operator("uv.mark_seam", text="Clear Seam").clear = True
        
        col = box.column(align=True)
        row = col.row(align=True)
        uv_island_pack = row.operator("uv.pack_islands", text="Pack Islands")
        uv_island_pack.margin = 0.1
        row = col.row(align=True)
        uv_island_pack = row.operator("uv.average_islands_scale", text='Average Scale')
        row = col.row(align=True)
        uv_island_pack = row.operator("uv.minimize_stretch", text='Relax')


        col = box.column(align=True)
        row = col.row(align=True)
        row.operator("uv.stitch")
        row.operator("uv.select_split", text="Disconnect")

        col = box.column(align=True)
        row = col.row(align=True)
        mir = row.operator("transform.mirror", text="Mirror X")
        mir.constraint_axis=(True, False, False)
        mir = row.operator("transform.mirror", text="Mirror Y")
        mir.constraint_axis=(False, True, False)
        row = col.row(align=True)
        col.label(text="Project Selected UV")
        row = col.row(align=True)
        row.operator("bsi.uv_project_planar", text='Top').direction='TOP'
        row.operator("bsi.uv_project_planar", text='Front').direction='FRONT'
        row.operator("bsi.uv_project_planar", text='Side').direction='RIGHT'
        row.operator("bsi.uv_project_planar", text='Normal')

        # bpy.ops.transform.mirror(constraint_axis=(True, False, False), constraint_orientation='GLOBAL', proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
        # py.ops.uv.unwrap(method='ANGLE_BASED', fill_holes=True, correct_aspect=True, use_subsurf_data=False, margin=0.001)
        # row.prop(uv_island_pack, "margin")
        # row.prop(uv_island_pack, "rotate")

        #col.prop(uv_island_pack, "clip_to_bounds", text="Normalized")
        #bpy.ops.uv.smart_project(angle_limit=66, island_margin=0, user_area_weight=0, use_aspect=True)
        box =layout.box()
        split = box.split()
        col = split.column(align=True)
        col.label(text="Align UV")
        row = col.row(align=True)
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='SCULPT_DYNTOPO')
        uv_op.top        = True
        uv_op.left       = True
        uv_op.right      = False
        uv_op.bottom     = False
        uv_op.vertical   = False
        uv_op.horizontal = False   
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='TRIA_UP')
        uv_op.top        = True
        uv_op.left       = False
        uv_op.right      = False
        uv_op.bottom     = False
        uv_op.vertical   = False
        uv_op.horizontal = False
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='SCULPT_DYNTOPO')
        uv_op.top        = True
        uv_op.left       = False
        uv_op.right      = True
        uv_op.bottom     = False
        uv_op.vertical   = False
        uv_op.horizontal = False

        row = col.row(align=True)
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='TRIA_LEFT')
        uv_op.top        = False
        uv_op.left       = True
        uv_op.right      = False
        uv_op.bottom     = False
        uv_op.vertical   = False
        uv_op.horizontal = False

        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='LINK')
        uv_op.top        = False
        uv_op.left       = False
        uv_op.right      = False
        uv_op.bottom     = False
        uv_op.vertical   = True
        uv_op.horizontal = True

        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='TRIA_RIGHT')
        uv_op.top        = False
        uv_op.left       = False
        uv_op.right      = True
        uv_op.bottom     = False
        uv_op.vertical   = False
        uv_op.horizontal = False

        row = col.row(align=True)
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='SCULPT_DYNTOPO')
        uv_op.top        = False
        uv_op.left       = True
        uv_op.right      = False
        uv_op.bottom     = True
        uv_op.vertical   = False
        uv_op.horizontal = False

        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='TRIA_DOWN')     
        uv_op.top        = False
        uv_op.left       = False
        uv_op.right      = False
        uv_op.bottom     = True
        uv_op.vertical   = False
        uv_op.horizontal = False

        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='SCULPT_DYNTOPO')
        uv_op.top        = False
        uv_op.left       = False
        uv_op.right      = True
        uv_op.bottom     = True
        uv_op.vertical   = False
        uv_op.horizontal = False
        
        row = col.row(align=True)
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='RIGHTARROW')
        uv_op.top        = False
        uv_op.left       = False
        uv_op.right      = False
        uv_op.bottom     = False
        uv_op.vertical   = True
        uv_op.horizontal = False
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='SCULPT_DYNTOPO', emboss=True)
        uv_op = row.operator("bsi.uv_align_bounds", text="", icon='DOWNARROW_HLT')
        uv_op.top        = False
        uv_op.left       = False
        uv_op.right      = False
        uv_op.bottom     = False
        uv_op.vertical   = False
        uv_op.horizontal = True
        
        col = split.column(align=False)
        col.label(text="Distribute")
        row = col.row(align=True)
        uv_op = row.operator("bsi.uv_distribute_islands", text="X") 
        uv_op.x = True
        uv_op.y = False
        row.prop(uv_op, "spacing")
        
        row = col.row(align=True)
        uv_op = row.operator("bsi.uv_distribute_islands", text="Y") 
        uv_op.x = False
        uv_op.y = True
        row.prop(uv_op, "spacing")
        row = col.row(align=True)
        uv_op = row.operator("bsi.align_island_to_selected", text="Aling Selected") 
       
def register():
    bpy.utils.register_class(BSI_UV_tools)


def unregister():
    bpy.utils.unregister_class(BSI_UV_tools)


if __name__ == "__main__":
    register()