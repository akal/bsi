from bpy.types import Operator

class BsiButtonDecorator(Operator):
    """Bsi button decorator"""
    bl_idname = "wm.bsi_decorator_button"
    bl_label = "BSI Button"
    bl_options = {'REGISTER'}

    def execute(self, context):
        print ("I am just a decorator")
        return {'FINISHED'}
