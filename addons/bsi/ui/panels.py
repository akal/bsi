import bpy
from bpy import context
from bpy.types import Panel #Menu, Panel, UIList
from bpy.app.translations import contexts as cntx
from ..libs import gui_utilities
from . import panel_layouts as PANEL

############################### CONSTANTS ###############################
GUI = gui_utilities.GuiUtilities()
############################### CONSTANTS ###############################

class View3DPanel:
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'TOOLS'
# class VIEW3D_PT_bsi_modeling(View3DPanel, Panel):
# 	bl_category = "BSI"
# 	bl_context = ""
# 	# bl_context = "objectmode"
# 	bl_label = " "
# 	def draw_header(self, context):
# 		layout = self.layout
# 		layout.operator("bsi.view3d_dynamic_prop_panel", icon='RADIO').panel="BSI_PP_home"
	
# 	def draw(self, context):
# 		pass

# class VIEW3D_PT_bsi_simulate(View3DPanel, Panel):
# 	bl_category = "Animation"
# 	bl_context = ""
# 	# bl_context = "objectmode"
# 	bl_label = "Simulate"

# 	def draw(self, context):
# 		layout = self.layout 
		
# class VIEW3D_PT_bsi_particles(View3DPanel, Panel):
# 	bl_category = "Particles"
# 	bl_context = ""
# 	# bl_context = "objectmode"
# 	bl_label = "Particles"

# 	def draw(self, context):
# 		layout = self.layout
# 		col = layout.column(align=True)
# 		col.operator("wm.bsi_decorator_button",text="Get")
# 		col.menu("INFO_MT_bsi_primitive", text="Primitive")
# 		col.menu("INFO_MT_bsi_material", text="Force")
# 		col.menu("INFO_MT_bsi_property", text="Property")
		
# 		layout = self.layout
# 		col = layout.column(align=True)
# 		col.operator("wm.bsi_decorator_button",text="Create")
# 		col.menu("INFO_MT_add", text="Particles")
# 		col.menu("INFO_MT_add", text="Rigid Body")
# 		col.menu("INFO_MT_add", text="Soft Body")
# 		col.menu("INFO_MT_add", text="Cloth")

# 		layout = self.layout
# 		col = layout.column(align=True)
# 		col.operator("wm.bsi_decorator_button",text="Modify")
# 		col.menu("INFO_MT_add", text="Particles")
# 		col.menu("INFO_MT_add", text="Rigid Body")
# 		col.menu("INFO_MT_add", text="Environment")
# 		col.menu("INFO_MT_add", text="Shader")
# 		col.menu("INFO_MT_add", text="Deform")

# 		layout = self.layout
# 		col = layout.column(align=True)
# 		col.operator("wm.bsi_decorator_button",text="Inspect")
# 		col.menu("INFO_MT_add", text="Par Types")
# 		col.menu("INFO_MT_add", text="Obstacles")
# 		col.menu("INFO_MT_add", text="Emmisions")
# 		col.menu("INFO_MT_add", text="Events")
# 		col.menu("INFO_MT_add", text="Forces")
# 		col.menu("INFO_MT_add", text="Goals")

# class VIEW3D_PT_bsi_modeling(View3DPanel, Panel):
# 	bl_category = "Modeling"
# 	bl_context = ""
# 	# bl_context = "objectmode"
# 	bl_label = " "
# 	def draw_header(self, context):
# 		layout = self.layout
# 		#layout.operator("bsi.view3d_dynamic_prop_panel", icon='RADIO').panel="BSI_PP_home"
# 		col.operator_menu_enum('view_3d.my_operator', 'this_method', 'Modeling')
# 	def draw(self, context):
# 		layout = self.layout
# 		bsi_modeling_panel


def VIEW3D_PT_view3d_properties(layout):
	view = GUI.get_screen_area()
	col = layout.column()
	col.active = bool(view.region_3d.view_perspective != 'CAMERA' or view.region_quadviews)
	col.prop(view, "lens")
	col.label(text="Lock to Object:")
	col.prop(view, "lock_object", text="")
	lock_object = view.lock_object
	if lock_object:
		if lock_object.type == 'ARMATURE':
			col.prop_search(view, "lock_bone", lock_object.data,
							"edit_bones" if lock_object.mode == 'EDIT'
							else "bones",
							text="")
	else:
		col.prop(view, "lock_cursor", text="Lock to Cursor")

	col = layout.column()
	col.prop(view, "lock_camera")

	col = layout.column(align=True)
	col.label(text="Clip:")
	col.prop(view, "clip_start", text="Start")
	col.prop(view, "clip_end", text="End")

	subcol = col.column(align=True)
	subcol.enabled = not view.lock_camera_and_layers
	subcol.label(text="Local Camera:")
	subcol.prop(view, "camera", text="")

	col = layout.column(align=True)
	col.prop(view, "use_render_border")
	col.active = view.region_3d.view_perspective != 'CAMERA'	   

def VIEW3D_PT_view3d_cursor(layout):
	view = GUI.get_screen_area()
	layout.column().prop(view, "cursor_location", text="Location")


def bsi_empty_panel(layout):
	pass


def bl_shading_panel(layout):
	context = bpy.context
	view = context.space_data
	scene = context.scene
	gs = scene.game_settings
	obj = context.object

	col = layout.column()

	if not scene.render.use_shading_nodes:
		col.prop(gs, "material_mode", text="")

	if view.viewport_shade == 'SOLID':
		col.prop(view, "show_textured_solid")
		col.prop(view, "use_matcap")
		if view.use_matcap:
			col.template_icon_view(view, "matcap_icon")
	elif view.viewport_shade == 'TEXTURED':
		if scene.render.use_shading_nodes or gs.material_mode != 'GLSL':
			col.prop(view, "show_textured_shadeless")

	col.prop(view, "show_backface_culling")

	if view.viewport_shade not in {'BOUNDBOX', 'WIREFRAME'}:
		if obj and obj.mode == 'EDIT':
			col.prop(view, "show_occlude_wire")

	fx_settings = view.fx_settings

	if view.viewport_shade not in {'BOUNDBOX', 'WIREFRAME'}:
		sub = col.column()
		sub.active = view.region_3d.view_perspective == 'CAMERA'
		sub.prop(fx_settings, "use_dof")
		col.prop(fx_settings, "use_ssao", text="Ambient Occlusion")
		if fx_settings.use_ssao:
			ssao_settings = fx_settings.ssao
			subcol = col.column(align=True)
			subcol.prop(ssao_settings, "factor")
			subcol.prop(ssao_settings, "distance_max")
			subcol.prop(ssao_settings, "attenuation")
			subcol.prop(ssao_settings, "samples")
			subcol.prop(ssao_settings, "color")


def bsi_material_panel(layout):
		context = bpy.context
		obj = context.object
		settings = context.tool_settings
		cursor_set = GUI.get_view3d_cursor()
		view = context.space_data
			
		col = layout.column(align=True)
		col.menu("VIEW3D_MT_select_menu", text="Select")
		row = col.row(align=True)

def BSI_PP_home(layout):
		context = bpy.context
		obj = context.object
		settings = context.tool_settings
		cursor_set = GUI.get_view3d_cursor()
		view = context.space_data
			
		col = layout.column(align=True)
		col.menu("VIEW3D_MT_select_menu", text="Select")
		row = col.row(align=True)

		row.operator("wm.bsi_decorator_button", text="Group")
		row.operator("wm.bsi_decorator_button", text="Center")
		
		row = col.row(align=True)
		row.operator("bsi.view3d_object_mode", text="Object", icon='MESH_CUBE')
		row.operator("bsi.view3d_vertex_mode", text="Point", icon='VERTEXSEL')
		row = col.row(align=True)
		row.operator("bsi.view3d_edge_mode", text="Edge",icon='EDGESEL')
		row.operator("bsi.view3d_face_mode", text="Polygon", icon='FACESEL')

		row = col.row(align=True)
		row.menu("INFO_MT_add", text="Explore")
		row.menu("INFO_MT_add", text="Scene")
		row = col.row(align=True)
		row.menu("INFO_MT_add", text="Selection")
		row.menu("INFO_MT_add", text="Clusters")

		split = col.split(percentage=0.85, align=True)
		split.operator("wm.bsi_decorator_button", text="Sample")
		col = split.column(align=True)
		col.menu("INFO_MT_add", text="")
		if obj != None:
			col = layout.column(align=True)
			col.prop(obj, "name", text="")
			col.prop(obj, "data", text="Data ")

			# TRANFORMS SECION
			layout.menu("VIEW3D_BSI_transform_menu", text="Transforms")
			split = layout.split(percentage=0.7)
			split.column().prop(obj, "scale", text="")
			col = split.column(align=True)
			col.operator("wm.bsi_decorator_button", text="X")
			col.operator("wm.bsi_decorator_button", text="Y")
			col.operator("wm.bsi_decorator_button", text="Z")  
			col = split.column(align=True)
			col.operator("wm.bsi_decorator_button", text="S")
			col.label(text="")
			col.operator("wm.bsi_decorator_button", text="", icon='COLLAPSEMENU') 
			
			split = layout.split(percentage=0.7)
			split.column().prop(obj, "rotation_euler", text="")
			col = split.column(align=True)
			col.operator("wm.bsi_decorator_button", text="X")
			col.operator("wm.bsi_decorator_button", text="Y")
			col.operator("wm.bsi_decorator_button", text="Z")  
			col = split.column(align=True)
			col.operator("wm.bsi_decorator_button", text="R")
			col.label(text="")
			col.operator("wm.bsi_decorator_button", text="", icon='COLLAPSEMENU') 
			
			split = layout.split(percentage=0.7)
			split.column().prop(obj, "location", text="")
			col = split.column(align=True)
			col.operator("wm.bsi_decorator_button", text="X")
			col.operator("wm.bsi_decorator_button", text="Y")
			col.operator("wm.bsi_decorator_button", text="Z")  
			col = split.column(align=True)
			col.operator("transform.translate", text="T")
			col.label(text="")
			col.operator("wm.bsi_decorator_button", text="", icon='COLLAPSEMENU')

		# TRANFORMS SUB SECION
		col = layout.column(align=True)
		row = col.row(align=True)
		row.alert =True
		# row.prop(cursor_set, "cursor_visible", text="", toggle=True,
		# 		 icon=('RESTRICT_VIEW_OFF' if cursor_set.cursor_visible
		# 			   else 'RESTRICT_VIEW_ON'))
		row.alert = False
		row.prop(view, "transform_orientation", text="")
		row.operator("wm.bsi_decorator_button", text="ref")    
		row.operator("transform.create_orientation", text="Create")    
		# row.menu("INFO_MT_add", text="Plane") 
		row = col.row(align=True)
		row.operator("wm.bsi_decorator_button", text="COG")
		# row.operator_enum("space_data.pivot_point", "type")
		row.prop_menu_enum(settings, "proportional_edit")
		row.menu("VIEW3D_MT_symmetry_menu", text="Sym")     

		#SNAP SECION
		col = layout.column(align=True)
		col.menu("VIEW3D_MT_CursorMenu", text="Snap")
		row = col.row(align=True)
		row.operator("wm.bsi_decorator_button", text="ON")
		row.operator("wm.bsi_decorator_button", text="", icon = 'DOT')     
		row.operator("wm.bsi_decorator_button", text="", icon='CURVE_DATA') 
		row.operator("wm.bsi_decorator_button", text="", icon='GRID') 
		
		#CONSTRAINTS SECION
		col = layout.column(align=True)
		col.menu("INFO_MT_add", text="Constraint")
		row = col.row(align=True)
		row.operator("object.parent_set", text="Parent")
		row.operator("object.parent_clear", text="Cut")
		row = col.row(align=True)
		row.operator("wm.bsi_decorator_button", text="CnsComp")
		row.operator("wm.bsi_decorator_button", text="ChldComp")

		#EDIT SECION
		col = layout.column(align=True)
		col.menu("INFO_MT_add", text="Edit")
		row = col.row(align=True)
		row.operator("wm.bsi_decorator_button", text="Freeze")
		row.operator("wm.bsi_decorator_button", text="Group")
		row = col.row(align=True)
		row.operator("wm.bsi_decorator_button", text="Freeze M")
		row.operator("wm.bsi_decorator_button", text="Immed")



def OBJECT_PT_object_properties(layout):
	obj = context.object
	obj_type = obj.type
	is_geometry = (obj_type in {'MESH', 'CURVE', 'SURFACE', 'META', 'FONT'})
	is_wire = (obj_type in {'CAMERA', 'EMPTY'})
	is_empty_image = (obj_type == 'EMPTY' and obj.empty_draw_type == 'IMAGE')
	is_dupli = (obj.dupli_type != 'NONE')
	view = GUI.get_screen_area()
	row = layout.row()
	row.template_ID(context.scene.objects, "active")

	split = layout.split()
	col = split.column()
	if is_wire:
		# wire objects only use the max. draw type for duplis
		col.active = is_dupli
		col.label(text="Maximum Dupli Draw Type:")
	else:
		col.label(text="Maximum Draw Type:")
	col.prop(obj, "draw_type", text="")

	col = split.column()
	if is_geometry or is_empty_image:
		# Only useful with object having faces/materials...
		col.label(text="Object Color:")
		col.prop(obj, "color", text="")
		

	split = layout.split()
	col = split.column()
	col.prop(obj, "show_name", text="Name")
	col.prop(obj, "show_axis", text="Axis")
	# Makes no sense for cameras, armatures, etc.!
	# but these settings do apply to dupli instances
	if is_geometry or is_dupli:
		col.prop(obj, "show_wire", text="Wire")
	if obj_type == 'MESH' or is_dupli:
		col.prop(obj, "show_all_edges")

	col = split.column()
	row = col.row()
	row.prop(obj, "show_bounds", text="Bounds")
	sub = row.row()
	sub.active = obj.show_bounds
	sub.prop(obj, "draw_bounds_type", text="")

	if is_geometry:
		col.prop(obj, "show_texture_space", text="Texture Space")
	col.prop(obj, "show_x_ray", text="X-Ray")
	if obj_type == 'MESH' or is_empty_image:
		col.prop(obj, "show_transparent", text="Transparency")
