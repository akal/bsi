import bpy
from bpy.props import *
# from ..libs import gui_utilities
from ..ui import panel_layouts as PANEL

############################### CONSTANTS ###############################
# GUI = gui_utilities.GuiUtilities()
############################### CONSTANTS ###############################

#dynamic_menu_id = 'None'

class BSI_dynamic_tool_panel(bpy.types.Panel):
	bl_label = "Tools"
	bl_idname = "BSI_dynamic_tool_panel"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	bl_category = "Tools"

	panel = "VIEW3D_TP_addobject"

	# def draw_header(self, context):
	# 	layout = self.layout
	# 	layout.operator_menu_enum("bsi.view3d_dynamic_tool_panel", "panel_choices", "%s" % self.panel )#Here the operator menu is displayed
	
	def draw(self, context):
		layout = self.layout
		dyn_menu = "PANEL.%s(layout, context)" % self.panel
		exec (dyn_menu)

class VIEW3D_OT_dynamic_tool_panel_op(bpy.types.Operator):#When an option in the operator menu is clicked, this is called
	"""BSI dynamic tools panel exec"""
	bl_idname = "bsi.view3d_dynamic_tool_panel"
	bl_label = ""
	bl_options = {'REGISTER'}

	panel = StringProperty(  
							name="panel",  
							default="",  
							description="string of a method panel" 
							)

	def execute(self, context):
		dyn = bpy.types.BSI_dynamic_tool_panel
		bpy.utils.unregister_class(dyn)
		dyn.panel = self.panel
		bpy.utils.register_class(dyn)
		return {'FINISHED'}

