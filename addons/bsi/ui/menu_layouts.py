import bpy
from bpy.app.translations import contexts as cntx
from ..libs import gui_utilities
from ..libs import mesh_utilities

############################### CONSTANTS ###############################
GUI = gui_utilities.GuiUtilities()
MSH = mesh_utilities.MeshUtilities()
############################### CONSTANTS ###############################

######################################################################
#########################  TRANSFORM MENUs ###########################
######################################################################
def object_reset_xform_menu(layout):
	props = layout.operator("bsi.object_xform_reset", text="Reset All Transforms", text_ctxt=cntx.default)
	props.scale, props.rotation, props.location = True, True, True
	props = layout.operator("bsi.object_xform_reset", text="Reset Scaling", text_ctxt=cntx.default)
	props.scale, props.rotation, props.location = True, False, False
	props = layout.operator("bsi.object_xform_reset", text="Reset Rotation", text_ctxt=cntx.default)
	props.scale, props.rotation, props.location = False, True, False
	props = layout.operator("bsi.object_xform_reset", text="Reset Translation", text_ctxt=cntx.default)
	props.scale, props.rotation, props.location = False, False, True

def object_freeze_xform_menu(layout):
	props = layout.operator("object.transform_apply", text="Freeze All Transforms", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = True, True, True
	props = layout.operator("object.transform_apply", text="Freeze Scaling", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = False, False, True
	props = layout.operator("object.transform_apply", text="Freeze Rotation", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = False, True, False
	props = layout.operator("object.transform_apply", text="Freeze Translation", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = True, False, False

def object_match_xform_menu(layout):
	props = layout.operator("bsi.object_xform_match", text="Match All Transforms", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = True, True, True
	props = layout.operator("bsi.object_xform_match", text="Match Scaling", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = False, False, True
	props = layout.operator("bsi.object_xform_match", text="Match Rotation", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = False, True, False
	props = layout.operator("bsi.object_xform_match", text="Match Translation", text_ctxt=cntx.default)
	props.location, props.rotation, props.scale = True, False, False

def object_menu(layout):
	layout.menu("VIEW3D_MT_add_menu", icon='OBJECT_DATAMODE')
	# layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	# layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.menu("VIEW3D_MT_ParentMenu", icon='ROTACTIVE')
	layout.menu("VIEW3D_MT_GroupMenu", icon='GROUP')
	layout.operator_menu_enum("object.modifier_add", "type", icon='MODIFIER')
	layout.menu("VIEW3D_MT_AlignMenu", icon='ALIGN')
	layout.menu("VIEW3D_MT_select_menu", icon='RESTRICT_SELECT_OFF')
	layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	layout.operator("view3d.properties", icon='MENU_PANEL')
	#TODO: Add if statement to test whether editmode switch needs to
	#be added to the menu, since certain object can't enter edit mode
	#In which case we don't need the toggle
	layout.operator("object.editmode_toggle", text="Enter Edit Mode", icon='EDITMODE_HLT')
	layout.operator("bsi.mesh_delete", text="Delete Object", icon='CANCEL')

def selection_menu(layout):
	layout.operator("bsi.object_select_tree", text="Select Tree") 
	layout.operator("mesh.loop_select", text="Select Edge Loop") 
	layout.operator("mesh.edgering_select", text="Select Edge Ring") 
	# layout.operator("wm.bsi_decorator_button", text="Select Tree") 
	# layout.operator("wm.bsi_decorator_button", text="Select Model") 
	# layout.operator("wm.bsi_decorator_button", text="Select Materials used by") 
	# layout.operator("wm.bsi_decorator_button", text="Select Edge Loop (no corners)") 
	# layout.operator("wm.bsi_decorator_button", text="Select Edge Lopp (around corners)") 
	# layout.operator("wm.bsi_decorator_button", text="Select Partial Edge Loop") 
def parent_menu(layout):
	layout.operator("object.parent_set", text="Set")
	layout.operator("object.parent_clear", text="Clear")

def group_menu(layout):
	layout.operator("group.create", text="Create Group") 
	# layout.operator("wm.bsi_decorator_button", text="Ungroup") 
	layout.operator("group.objects_remove", text="Remove From Group")
	layout.separator()
	layout.operator("group.objects_add_active", text="Add to Group") 
	layout.operator("group.objects_remove_active")

def group_transform_menu(layout):
	layout.operator("wm.bsi_decorator_button", text="Create Transform Group") 
	layout.operator("wm.bsi_decorator_button", text="Remove Transform Group") 

def cluster_menu(layout):
	layout.operator("wm.bsi_decorator_button", text="Create Cluster") 
	layout.operator("wm.bsi_decorator_button", text="Create Cluster w/Center") 
	layout.operator("wm.bsi_decorator_button", text="Create Non-Overlapping Clusters") 
	layout.operator("wm.bsi_decorator_button", text="Add To Cluster") 
	layout.operator("wm.bsi_decorator_button", text="Remove Cluster") 
	layout.operator("wm.bsi_decorator_button", text="Remove from Cluster")
 
def duplicate_menu(layout):
	layout.operator("object.duplicate_move", text="Duplicate") 
	layout.operator("object.duplicate_move_linked", text="Instantiate") 
	layout.operator("object.duplicate_move_linked", text="Freeze Instances")

def selection_global(layout):
	layout.operator("view3d.select_border")
	layout.operator("view3d.select_circle")

def selection_object_mode(layout):
	selection_global(layout)

	layout.separator()
	layout.operator("object.select_all").action = 'TOGGLE'
	layout.operator("object.select_all", text="Inverse").action = 'INVERT'
	layout.operator("object.select_random", text="Random")
	layout.operator("object.select_mirror", text="Mirror")
	layout.operator("object.select_by_layer", text="Select By Layer")
	layout.operator_menu_enum("object.select_by_type", "type", text="Select By Type")
	layout.operator("object.select_camera", text="Select Camera")

	layout.separator()
	layout.operator_menu_enum("object.select_grouped", "type", text="Grouped")
	layout.operator_menu_enum("object.select_linked", "type", text="Linked")
	layout.operator("object.select_pattern", text="Select Pattern...")

def selection_mesh_edit_mode(layout):
	context = bpy.context
	settings = bpy.context.tool_settings
	selection_global(layout)
	layout.separator()
	layout.operator("mesh.select_all").action = 'TOGGLE'
	layout.operator("mesh.select_all", text="Inverse").action = 'INVERT'
	layout.operator("mesh.select_random", text="Random")
	layout.operator("mesh.select_nth", text="Every N Number of Verts")
	layout.operator("mesh.edges_select_sharp", text="Sharp Edges")
	layout.operator("mesh.faces_select_linked_flat",
					text="Linked Flat Faces")
	layout.operator("mesh.select_interior_faces", text="Interior Faces")
	layout.operator("mesh.select_axis", text="Side of Active")
	layout.separator()
	layout.operator("mesh.select_face_by_sides", text="By Number of Verts")
	if settings.mesh_select_mode[2] == False:
		layout.operator("mesh.select_non_manifold", text="Non Manifold")
	layout.operator("mesh.select_loose", text="Loose Geometry")
	layout.operator("mesh.select_similar", text="Similar")
	layout.separator()
	layout.operator("mesh.select_less", text="Less")
	layout.operator("mesh.select_more", text="More")
	layout.separator()
	layout.operator("mesh.select_mirror", text="Mirror")
	layout.operator("mesh.select_linked", text="Linked")
	layout.operator("mesh.shortest_path_select", text="Shortest Path")
	layout.operator("mesh.loop_multi_select", text="Edge Loop")
	layout.operator("mesh.loop_multi_select", text="Edge Ring").ring = True
	layout.separator()
	layout.operator("mesh.loop_to_region")
	layout.operator("mesh.region_to_loop")


def selection_curve_menu(layout):
	selection_global(layout)
	layout.separator()
	layout.operator("curve.select_all").action = 'TOGGLE'
	layout.operator("curve.select_all").action = 'INVERT'
	layout.operator("curve.select_random")
	layout.operator("curve.select_nth")
	layout.separator()
	layout.operator("curve.de_select_first")
	layout.operator("curve.de_select_last")
	layout.operator("curve.select_next")
	layout.operator("curve.select_previous")
	layout.separator()
	layout.operator("curve.select_more")
	layout.operator("curve.select_less")

def selection_armature_menu(layout):
	selection_global(layout)
	layout.separator()
	layout.operator("armature.select_all")
	layout.operator("armature.select_inverse", text="Inverse")
	layout.separator()
	layout.operator("armature.select_hierarchy",
							text="Parent").direction = 'PARENT'
	layout.operator("armature.select_hierarchy",
							text="Child").direction = 'CHILD'
	layout.separator()
	props = layout.operator("armature.select_hierarchy",
							text="Extend Parent")
	props.extend = True
	props.direction = 'PARENT'
	props = layout.operator("armature.select_hierarchy",
							text="Extend Child")
	props.extend = True
	props.direction = 'CHILD'
	layout.operator("object.select_pattern", text="Select Pattern...")

def selection_pose_menu(layout):
	selection_global(layout)
	layout.separator()
	layout.operator("pose.select_all").action = 'TOGGLE'
	layout.operator("pose.select_all", text="Inverse").action = 'INVERT'
	layout.operator("pose.select_constraint_target",
					text="Constraint Target")
	layout.operator("pose.select_linked", text="Linked")
	layout.separator()
	layout.operator("pose.select_hierarchy",
					text="Parent").direction = 'PARENT'
	layout.operator("pose.select_hierarchy",
					text="Child").direction = 'CHILD'
	layout.separator()
	props = layout.operator("pose.select_hierarchy", text="Extend Parent")
	props.extend = True
	props.direction = 'PARENT'
	props = layout.operator("pose.select_hierarchy", text="Extend Child")
	props.extend = True
	props.direction = 'CHILD'
	layout.separator()
	layout.operator_menu_enum("pose.select_grouped", "type",
							  text="Grouped")
	layout.operator("object.select_pattern", text="Select Pattern...")

def pose_copy(layout):
		layout.operator("pose.copy")
		layout.operator("pose.paste")
		layout.operator("pose.paste",
						text="Paste X-Flipped Pose").flipped = True

def symmetry_menu(layout):

	layout.operator("transform.mirror", text="Interactive Mirror")
	layout.separator()

	layout.operator_context = 'INVOKE_REGION_WIN'

	props = layout.operator("transform.mirror", text="X Global")
	props.constraint_axis = (True, False, False)
	props.constraint_orientation = 'GLOBAL'

	props = layout.operator("transform.mirror", text="Y Global")
	props.constraint_axis = (False, True, False)
	props.constraint_orientation = 'GLOBAL'

	props = layout.operator("transform.mirror", text="Z Global")
	props.constraint_axis = (False, False, True)
	props.constraint_orientation = 'GLOBAL'

	if bpy.context.edit_object:

		layout.separator()

		props = layout.operator("transform.mirror", text="X Local")
		props.constraint_axis = (True, False, False)
		props.constraint_orientation = 'LOCAL'
		props = layout.operator("transform.mirror", text="Y Local")
		props.constraint_axis = (False, True, False)
		props.constraint_orientation = 'LOCAL'
		props = layout.operator("transform.mirror", text="Z Local")
		props.constraint_axis = (False, False, True)
		props.constraint_orientation = 'LOCAL'

		layout.operator("object.vertex_group_mirror")

def edge_tools_menu(layout):
	layout.operator("wm.bsi_decorator_button", text="Add Polygon Tool") 
	layout.operator("wm.bsi_decorator_button", text="Add Edge Tool")
	layout.operator("wm.bsi_decorator_button", text="Add Vertex Tool")
	layout.operator("mesh.knife_tool", text="Knife Tool") 
	layout.operator("wm.bsi_decorator_button", text="Split Edge Tool") 
	layout.operator("wm.bsi_decorator_button", text="Split Polygon Tool")
	layout.operator("wm.bsi_decorator_button", text="Weld Points Tool")
	layout.operator("wm.bsi_decorator_button", text="Tweak User Normals Tool")
	layout.operator("bsi.view3d_collapse", text="Merge")
	layout.operator("wm.bsi_decorator_button", text="Dice Polygons")
	layout.operator("wm.bsi_decorator_button", text="Subdivision")
	layout.operator("wm.bsi_decorator_button", text="Symmetrize") 

def transforms_menu(layout):
	context = bpy.context
	layout.operator("transform.translate", text="Grab/Move")
	# TODO: sub-menu for grab per axis
	layout.operator("transform.rotate", text="Rotate")
	# TODO: sub-menu for rot per axis
	layout.operator("transform.resize", text="Scale")
	# TODO: sub-menu for scale per axis
	layout.separator()

	layout.operator("transform.tosphere", text="To Sphere")
	layout.operator("transform.shear", text="Shear")
	layout.operator("transform.bend", text="Bend")
	layout.operator("transform.push_pull", text="Push/Pull")
	if context.edit_object and context.edit_object.type == 'ARMATURE':
		layout.operator("armature.align")
	else:
		layout.operator_context = 'EXEC_REGION_WIN'
		# @todo vvv See alignmenu() in edit.c of b2.4x to get this working.
		layout.operator("transform.transform",
						text="Align to Transform Orientation").mode = 'ALIGN'
	layout.separator()

	layout.operator_context = 'EXEC_AREA'

	layout.operator("object.origin_set",
					text="Geometry to Origin").type = 'GEOMETRY_ORIGIN'
	layout.operator("object.origin_set",
					text="Origin to Geometry").type = 'ORIGIN_GEOMETRY'
	layout.operator("object.origin_set",
					text="Origin to 3D Cursor").type = 'ORIGIN_CURSOR'
	layout.separator()
	transforms_orientation_menu(layout)

def transforms_orientation_menu(layout):
	layout.operator("space_data.transform_orientation", text="Global").type='GLOBAL'
	layout.operator("space_data.transform_orientation", text="Local").type='LOCAL'
	layout.operator("space_data.transform_orientation", text="Parent").type='NORMAL'
	layout.operator("space_data.transform_orientation", text="Gibal").type='GIMBAL'
	layout.operator("space_data.transform_orientation", text="View").type='VIEW'
	# layout.operator("space_data.transform_orientation", text="View").type='GIMBAL'

def misc_menu(layout):
	layout.operator("wm.bsi_decorator_button", text="COG (N/A)") 
	layout.operator("wm.context_toggle_enum", text="Prop")
	layout.menu("VIEW3D_MT_mirror_menu", text="Sym")

def delete_menu(layout):
	layout.operator("bsi.mesh_delete", text="Delete") 
	layout.menu("VIEW3D_MT_edit_mesh_delete", icon='SETTINGS')
	layout.operator("bsi.view3d_collapse", text="Collapse") 
	layout.operator_menu_enum("mesh.merge", "type", icon='SETTINGS')
	layout.operator("mesh.remove_doubles")

def knife_menu(layout, selection):
	props = layout.operator("mesh.knife_tool", text="Knife")
	props.use_occlude_geometry = True
	props.only_selected = False

	if len(selection)>0:
		props.use_occlude_geometry = False
		props.only_selected = True
		

def edge_menu(layout):
	r""" Taken from blender mesh edit layout"""
	sel = MSH.get_sub_selection(bpy.context.active_object)
	layout.separator()
	layout.operator("transform.edge_slide", text="Slide Edge")
	# layout.operator("transform.vert_slide", text="Slide Vertex")
	# layout.operator("mesh.noise")
	layout.operator("mesh.vertices_smooth")
	layout.operator("transform.vertex_random")
	layout.separator()

	# layout.menu("VIEW3D_MT_edit_mesh_extrude")
	layout.operator("mesh.bevel", text="Bevel").vertex_only = False
	layout.operator("view3d.edit_mesh_extrude_move_normal", text="Extrude")
	# layout.operator("view3d.edit_mesh_extrude_individual_move", text="Extrude Individual")
	# layout.operator("mesh.inset", text="Inset Faces")
	print(len(sel))
	if len(sel) > 1:
		layout.operator("mesh.edge_face_add", text="Make Face")
		layout.operator("mesh.bisect")

	layout.operator("mesh.subdivide", text="Split/Subdivide Edge")
	layout.operator("mesh.loopcut_slide")
	layout.operator("mesh.offset_edge_loops_slide", text="Duplicate Edge")
	# layout.operator("mesh.offset_edge_loops")
	layout.operator("mesh.duplicate_move", text="Duplicate")
	# layout = layout.layout(align=True)
	layout.operator("mesh.spin")
	layout.operator("mesh.screw")

	# layout.separator()
	# knife_menu(layout, sel)

	layout.separator()
	delete_menu(layout)

	# layout.operator("bpy.ops.bsi.dlg_rename_op", text="Rename") 
	# layout.operator("bsi.mesh_delete", text="Delete") 
	# layout.separator()
	# selection_menu(layout)
	# layout.separator()
	# layout.operator("object.duplicate_move", text="Duplicate") 
	# layout.operator("object.duplicate_move_linked", text="Instantiate") 
	# layout.operator("object.duplicates_make_real", text="Freeze Instances")
	# layout.separator()
	# layout.operator("group.create", text="Create Group") 
	# layout.operator("group.objects_add_active", text="Add to Group") 
	# layout.operator("group.objects_remove", text="Remove From Group") 
	# layout.separator()
	# layout.operator("wm.bsi_decorator_button", text="Info Selection") 
	# layout.operator("wm.bsi_decorator_button", text="Explore") 
	# layout.operator("wm.bsi_decorator_button", text="Open Attachements") 
	# layout.operator("wm.bsi_decorator_button", text="Inspect Material") 
	# layout.operator("wm.bsi_decorator_button", text="Last Operator Stack") 
	# layout.operator("wm.bsi_decorator_button", text="Properties") 
	# layout.separator()
	# layout.operator("wm.bsi_decorator_button", text="Select Constraining Objects") 
	# layout.operator("wm.bsi_decorator_button", text="Select Objects Constrained") 
	# layout.separator()
	# layout.operator("wm.bsi_decorator_button", text="Copy All Animation")
	# layout.operator("wm.bsi_decorator_button", text="Paste All Animation")
	# layout.separator()
	# layout.operator("wm.bsi_decorator_button", text="Set Thumbnail (N/A)")
	# layout.operator("wm.bsi_decorator_button", text="Set Thumbnail From Region(N/A)")
	# layout.operator("wm.bsi_decorator_button", text="Clear Thumbnail(N/A)")
	# layout.separator()
	# layout.operator("wm.bsi_decorator_button", text="Set User Keywords")
	# layout.separator()
	# edge_tools_menu(layout)
	# layout.separator()
	# layout.menu("INFO_MT_bsi_object", text="Xforms") 

def vert_menu(layout):
	r'''Vertex Context Menu'''
	sel = MSH.get_sub_selection(bpy.context.active_object)
	layout.separator()
	layout.operator("transform.vert_slide", text="Vertex")
	layout.operator("mesh.vertices_smooth")
	layout.operator("transform.vertex_random")
	
	layout.separator()
	if len(sel) > 2:
		layout.operator("mesh.edge_face_add")
	if len(sel) > 1:
		layout.operator("mesh.subdivide")
	layout.operator("mesh.loopcut_slide")
	layout.operator("mesh.loopcut")
	layout.operator("mesh.duplicate_move", text="Duplicate")
	layout.operator("mesh.vert_connect", text="Duplicate")
	
	layout.operator("mesh.bevel", text="Bevel").vertex_only = True
	layout.operator("mesh.spin")
	layout.operator("mesh.screw")
	layout.operator("mesh.flip_normals", text="Flip Normals")

	layout.separator()
	knife_menu(layout, sel)
	layout.separator()
	delete_menu(layout)


def face_menu(layout):
	r'''Face Context Menu'''
	sel = MSH.get_sub_selection(bpy.context.active_object)
	# layout.operator("bpy.ops.bsi.dlg_rename", text="Rename") 

	layout.separator()
	layout.operator("transform.vert_slide", text="Slide Selection")
	layout.operator("mesh.vertices_smooth", text= "Smooth")
	layout.operator("transform.vertex_random")

	layout.separator()
	layout.menu("VIEW3D_MT_edit_mesh_extrude")
	# layout.operator("view3d.edit_mesh_extrude_move_normal", text="Extrude Region")
	# layout.operator("view3d.edit_mesh_extrude_individual_move", text="Extrude Individual")
	layout.operator("mesh.inset", text="Inset Faces")
	layout.operator("mesh.flip_normals", text="Flip Normals")
	layout.operator("mesh.bevel", text="Bevel").vertex_only = False
	layout.operator("mesh.subdivide")
	layout.operator("mesh.loopcut_slide")
	layout.operator("mesh.offset_edge_loops_slide", text="Duplicate Outline Edge")
	layout.operator("mesh.quads_convert_to_tris", text="Triangulate")
	layout.operator("mesh.duplicate_move", text="Duplicate")
	layout.operator("mesh.split", text="Disconnect Face")
	layout.operator("bsi.mesh_detach", text="Detach Delete").delete = True
	layout.operator("bsi.mesh_detach", text="Detach Keep").delete = False
	layout.operator("mesh.bridge_edge_loops", text="Bridge")


	layout.separator()
	knife_menu(layout, sel)
	if len(sel) > 0:
		layout.operator("mesh.bisect", text="Slice")

	layout.separator()
	layout.operator("bsi.mesh_delete", text="Delete") 
	layout.menu("VIEW3D_MT_edit_mesh_delete", icon='SETTINGS')
	layout.operator("bsi.view3d_collapse", text="Collapse") 
	layout.operator_menu_enum("mesh.merge", "type", icon='SETTINGS')
	layout.operator("mesh.edge_face_add", text="Join Faces")
	layout.operator("mesh.remove_doubles")

def curve_menu(layout):
	context = bpy.context
	settings = bpy.context.tool_settings

	layout.menu("INFO_MT_curve_add", text="Add Curve",
				icon='OUTLINER_OB_CURVE')
	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.prop_menu_enum(settings, "proportional_edit",
						  icon="PROP_CON")
	layout.prop_menu_enum(settings, "proportional_edit_falloff",
						  icon="SMOOTHCURVE")
	layout.menu("VIEW3D_MT_EditCurveCtrlpoints",
				icon='CURVE_BEZCURVE')
	layout.menu("VIEW3D_MT_EditCurveSpecials",
				icon='MODIFIER')
	#Could use: VIEW3D_MT_select_edit_curve
	#Which is the default, instead of a hand written one, left it alone
	#for now though.
	layout.menu("VIEW3D_MT_SelectCurveMenu",
				icon='RESTRICT_SELECT_OFF')
	# layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	# layout.operator("view3d.properties", icon='MENU_PANEL')
	# layout.operator("object.editmode_toggle", text="Enter Object Mode",
	# 				icon='OBJECT_DATA')
	# layout.operator("curve.delete", text="Delete Object",
	# 				icon='CANCEL')
	# layout.separator()
	# curve_special(layout)
	# layout.separator()
	# layout.operator("curve.handle_type_set") 

def curve_special(layout):
	layout.operator("curve.subdivide")
	layout.operator("curve.switch_direction")
	layout.operator("curve.spline_weight_set")
	layout.operator("curve.radius_set")
	layout.operator("curve.smooth")
	layout.operator("curve.smooth_weight")
	layout.operator("curve.smooth_radius")
	layout.operator("curve.smooth_tilt")

def hook_menu(layout):
		layout.operator_context = 'EXEC_AREA'
		layout.operator("object.hook_add_newob")
		layout.operator("object.hook_add_selob").use_bone = False
		layout.operator("object.hook_add_selob", text="Hook to Selected Object Bone").use_bone = True

		if [mod.type == 'HOOK' for mod in context.active_object.modifiers]:
			layout.separator()
			layout.operator_menu_enum("object.hook_assign", "modifier")
			layout.operator_menu_enum("object.hook_remove", "modifier")
			layout.separator()
			layout.operator_menu_enum("object.hook_select", "modifier")
			layout.operator_menu_enum("object.hook_reset", "modifier")
			layout.operator_menu_enum("object.hook_recenter", "modifier")

def surface_menu(layout):
	context = bpy.context
	settings = bpy.context.tool_settings
	layout.menu("INFO_MT_surface_add", text="Add Surface", icon='OUTLINER_OB_SURFACE')
	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.prop_menu_enum(settings, "proportional_edit", icon="PROP_CON")
	layout.prop_menu_enum(settings, "proportional_edit_falloff", icon="SMOOTHCURVE")
	layout.menu("VIEW3D_MT_EditCurveSpecials", icon='MODIFIER')
	layout.menu("VIEW3D_MT_SelectSurface", icon='RESTRICT_SELECT_OFF')
	layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	layout.operator("view3d.properties", icon='MENU_PANEL')
	layout.operator("object.editmode_toggle", text="Enter Object Mode", icon='OBJECT_DATA')
	layout.operator("curve.delete", text="Delete Object", icon='CANCEL')

def metaball_menu(layout):
	context = bpy.context
	settings = context.tool_settings
	#layout.menu("INFO_MT_metaball_add", text="Add Metaball",
	layout.operator_menu_enum("object.metaball_add", "type",
							  text="Add Metaball",
							  icon='OUTLINER_OB_META')

	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')

	layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')

	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')

	layout.prop_menu_enum(settings, "proportional_edit",
										icon="PROP_CON")
	layout.prop_menu_enum(settings, "proportional_edit_falloff",
									icon="SMOOTHCURVE")

	layout.menu("VIEW3D_MT_SelectMetaball", icon='RESTRICT_SELECT_OFF')

	layout.operator("view3d.toolshelf", icon='MENU_PANEL')

	layout.operator("view3d.properties", icon='MENU_PANEL')

	layout.operator("object.editmode_toggle", text="Enter Object Mode",
												icon='OBJECT_DATA')

	layout.operator("mball.delete_metaelems", text="Delete Object",
												icon='CANCEL')

def lattice_menu(layout):
	context = bpy.context
	settings = context.tool_settings

	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')

	layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')

	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')

	layout.prop_menu_enum(settings, "proportional_edit",
						  icon= "PROP_CON")
	layout.prop_menu_enum(settings, "proportional_edit_falloff",
						  icon= "SMOOTHCURVE")
	layout.operator("lattice.make_regular")

	layout.menu("VIEW3D_MT_select_edit_lattice",
				icon='RESTRICT_SELECT_OFF')

	layout.operator("view3d.toolshelf", icon='MENU_PANEL')

	layout.operator("view3d.properties", icon='MENU_PANEL')

	layout.operator("object.editmode_toggle", text="Enter Object Mode",
					icon='OBJECT_DATA')

	#layout.operator("object.delete", text="Delete Object",

def particle_menu(layout):
	context = bpy.context
	settings = context.tool_settings

	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')

	layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')

	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')

	layout.prop_menu_enum(settings, "proportional_edit",
						  icon= "PROP_CON")
	layout.prop_menu_enum(settings, "proportional_edit_falloff",
						  icon= "SMOOTHCURVE")

	layout.menu("VIEW3D_MT_particle", icon='PARTICLEMODE')

	#Select Particle
	layout.menu("VIEW3D_MT_select_particle",
				icon='RESTRICT_SELECT_OFF')

	layout.menu("VIEW3D_MT_undoS", icon='ARROW_LEFTRIGHT')

	layout.operator("view3d.toolshelf", icon='MENU_PANEL')

	layout.operator("view3d.properties", icon='MENU_PANEL')

	layout.operator("object.mode_set", text="Enter Object Mode",
					icon='OBJECT_DATA')

	layout.operator("object.delete", text="Delete Object",
					icon='CANCEL')

def weights_menu(layout):
	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')

	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')

	layout.menu("VIEW3D_MT_paint_weight", icon='WPAINT_HLT')

	layout.menu("VIEW3D_MT_undoS", icon='ARROW_LEFTRIGHT')

	layout.operator("view3d.toolshelf", icon='MENU_PANEL')

	layout.operator("view3d.properties", icon='MENU_PANEL')

	layout.operator("object.mode_set", text="Enter Object Mode",
					icon='OBJECT_DATA')

def vcolor_menu(layout):
	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.operator("paint.vertex_color_set", icon='VPAINT_HLT')
	layout.menu("VIEW3D_MT_undoS", icon='ARROW_LEFTRIGHT')
	layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	layout.operator("view3d.properties", icon='MENU_PANEL')
	layout.operator("object.mode_set", text="Enter Object Mode", icon='OBJECT_DATA')

def painttexture_menu(layout):
	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.menu("VIEW3D_MT_undoS", icon='ARROW_LEFTRIGHT')
	layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	layout.operator("view3d.properties", icon='MENU_PANEL')
	layout.operator("object.mode_set", text="Enter Object Mode", icon='OBJECT_DATA')

def sclupt_menu(layout):
	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.menu("VIEW3D_MT_sculpt", icon='SCULPTMODE_HLT')
	layout.menu("VIEW3D_MT_undoS", icon='ARROW_LEFTRIGHT')
	layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	layout.operator("view3d.properties", icon='MENU_PANEL')
	layout.operator("object.mode_set", text="Enter Object Mode", icon='OBJECT_DATA')

def armature_menu(layout):
	context = bpy.context
	settings = context.tool_settings

	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	layout.menu("VIEW3D_MT_mirror_menu", icon='MOD_MIRROR')
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.prop_menu_enum(settings, "proportional_edit",icon="PROP_CON")
	layout.prop_menu_enum(settings, "proportional_edit_falloff",icon="SMOOTHCURVE")
	layout.separator()
	layout.menu("VIEW3D_MT_edit_armature_roll",icon='BONE_DATA')
	layout.menu("VIEW3D_MT_EditArmatureTK",icon='ARMATURE_DATA')
	layout.menu("VIEW3D_MT_ArmatureName",icon='NEW')
	layout.menu("VIEW3D_MT_ParentMenu", icon='ROTACTIVE')
	layout.menu("VIEW3D_MT_bone_options_toggle", text="Bone Settings")
	layout.menu("VIEW3D_MT_armature_specials", icon='MODIFIER')
	layout.menu("VIEW3D_MT_SelectArmatureMenu",icon='RESTRICT_SELECT_OFF')
	layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	layout.operator("view3d.properties", icon='MENU_PANEL')
	layout.operator("object.posemode_toggle", text="Enter Pose Mode", icon='POSE_HLT')
	layout.operator("object.editmode_toggle", text="Enter Object Mode", icon='OBJECT_DATA')
	layout.operator("armature.delete", text="Delete Object", icon='CANCEL')

def pose_menu(layout):
	arm = context.active_object.data
	layout.menu("VIEW3D_MT_transform_menu", icon='MANIPUL')
	layout.menu("VIEW3D_MT_pose_transform")
	layout.menu("VIEW3D_MT_CursorMenu", icon='CURSOR')
	layout.menu("VIEW3D_MT_PoseCopy", icon='FILE')

	if arm.draw_type in {'BBONE', 'ENVELOPE'}:
		layout.operator("transform.transform", text="Scale Envelope Distance").mode = 'BONE_SIZE'

	layout.menu("VIEW3D_MT_pose_apply")
	layout.operator("pose.relax")
	layout.menu("VIEW3D_MT_KeyframeMenu")
	layout.menu("VIEW3D_MT_pose_pose")
	layout.menu("VIEW3D_MT_pose_motion")
	layout.menu("VIEW3D_MT_pose_group")
	layout.menu("VIEW3D_MT_pose_ik")
	layout.menu("VIEW3D_MT_PoseNames")
	layout.menu("VIEW3D_MT_pose_constraints")
	layout.operator("pose.quaternions_flip")
	layout.operator_context = 'INVOKE_AREA'
	layout.operator("armature.armature_layers", text="Change Armature Layers...")
	layout.operator("pose.bone_layers", text="Change Bone Layers...")
	layout.menu("VIEW3D_MT_pose_showhide")
	layout.menu("VIEW3D_MT_bone_options_toggle", text="Bone Settings")
	
	layout.menu("VIEW3D_MT_SelectPoseMenu", icon='RESTRICT_SELECT_OFF')
	layout.operator("view3d.toolshelf", icon='MENU_PANEL')
	layout.operator("view3d.properties", icon='MENU_PANEL')
	layout.operator("object.editmode_toggle", text="Enter Edit Mode", icon='EDITMODE_HLT')
	layout.operator("object.mode_set", text="Enter Object Mode", icon='OBJECT_DATA').mode='OBJECT'
