import bpy
from bpy.props import StringProperty
from . import panel_layouts as PANEL



def _callback(scene, prop):
	print("Property '%s' of %s updated to value %s" % (prop, scene.name, getattr(scene, prop)))

def test(prop):
	def update(self, context):
		_callback(self, prop)
	return update

# class BSI_dynamic_prop_panel(bpy.types.Panel):
# 	bl_label = " "
# 	bl_idname = "BSI_dynamic_prop_panel"
# 	bl_space_type = "VIEW_3D"
# 	bl_region_type = "UI"
	
# 	func = PANEL.BSI_PP_home

# 	def draw_header(self, context)
#: 		pass # layout = self.layout
# 		# layout.operator("bsi.view3d_dynamic_prop_panel", icon='RADIO').panel = self.func

# 	def draw(self, context):
# 		dyn_panel = "%s(self.layout, context)" % (self.func)
# 		print(dyn_panel)
# 		#exec (dyn_panel)

class BSI_dynamic_prop_panel(bpy.types.Panel):
	bl_label = " "
	bl_idname = "BSI_dynamic_prop_panel"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	
	panel = "BSI_PP_home"

	def draw_header(self, context):
		layout = self.layout
		layout.operator("bsi.view3d_dynamic_prop_panel", icon='RADIO').panel="BSI_PP_home"

	def draw(self, context):
		layout = self.layout
		dyn_panel = "PANEL.%s(layout, context)" % self.panel
		exec (dyn_panel)

class BSI_VIEW3D_dynamic_prop_panel_op(bpy.types.Operator):
	"""BSI dynamic props panel exec"""
	bl_idname = "bsi.view3d_dynamic_prop_panel"
	bl_label = ""
	bl_options = {'REGISTER'}

	panel = StringProperty(  
							name="panel",  
							default="",  
							description="string of a method panel" 
							)

	def execute(self, context):
		dyn = bpy.types.BSI_dynamic_prop_panel
		bpy.utils.unregister_class(dyn)
		dyn.panel = self.panel
		bpy.utils.register_class(dyn)

		return {'FINISHED'}

