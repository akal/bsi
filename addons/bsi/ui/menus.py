
import bpy
from bpy.types import Menu 
from . import menu_layouts as MENU
from . import panel_layouts as PANEL
from ..libs import gui_utilities

############################### CONSTANTS ###############################
GUI = gui_utilities.GuiUtilities()
############################### CONSTANTS ###############################

class BSI_ContextMenu(bpy.types.Menu):
	bl_label = "BSI Context Menu"

	def draw(self, context):
		layout = self.layout
		settings = context.tool_settings
		layout.operator_context = 'INVOKE_REGION_WIN'
		# Search Menu
		layout.operator("wm.search_menu", text="Search", icon='VIEWZOOM')
		layout.operator("bsi.view3d_dynamic_prop_panel",text="Home Panel", icon='RADIO').panel="BSI_PP_home"
		layout.operator("bsi.view3d_dynamic_prop_panel",text="Edit Material", icon='MATERIAL_DATA' ).panel="material_panel"

		ob = context
		if context.mode == 'OBJECT':
			MENU.object_menu(layout)
		
		elif context.mode == 'EDIT_MESH':
			sub_mod = settings.mesh_select_mode
			print(sub_mod)
			if sub_mod[0] == True: #Verts
				MENU.vert_menu(layout)
			if sub_mod[1] == True: #Edge
				MENU.edge_menu(layout)
			if sub_mod[2] == True: #Face
				MENU.face_menu(layout)

		if context.mode == 'EDIT_CURVE':
			MENU.curve_menu(layout)

		if context.mode == 'EDIT_SURFACE':
			MENU.surface_menu(layout)

		if context.mode == 'EDIT_METABALL':
			MENU.metaball_menu(layout)

		elif context.mode == 'EDIT_LATTICE':
			MENU.lattice_menu(layout)

		if  context.mode == 'PARTICLE':
			MENU.particle_menu(layout)

		if context.mode == 'PAINT_WEIGHT':
			MENU.weights_menu(layout)

		elif context.mode == 'PAINT_VERTEX':
			MENU.vcolor_menu(layout)

		elif context.mode == 'PAINT_TEXTURE':
			MENU.painttexture_menu(layout)

		elif context.mode == 'SCULPT':
			MENU.sclupt_menu(layout)

		elif context.mode == 'EDIT_ARMATURE':
			MENU.armature_menu(layout)

		if context.mode == 'POSE':
			MENU.pose_menu(layout)

class VIEW3D_MT_add_menu(bpy.types.Menu):
	bl_label = "Add Object Menu"

	def draw(self, context):
		layout = self.layout
		layout.operator_context = 'INVOKE_REGION_WIN'

		layout.menu("INFO_MT_mesh_add", text="Add Mesh",
					icon='OUTLINER_OB_MESH')
		layout.menu("INFO_MT_curve_add", text="Add Curve",
					icon='OUTLINER_OB_CURVE')
		layout.menu("INFO_MT_surface_add", text="Add Surface",
					icon='OUTLINER_OB_SURFACE')
		layout.operator_menu_enum("object.metaball_add", "type",
								  icon='OUTLINER_OB_META')
		layout.operator("object.text_add", text="Add Text",
						icon='OUTLINER_OB_FONT')
		layout.separator()
		layout.menu("INFO_MT_armature_add", text="Add Armature",
					icon='OUTLINER_OB_ARMATURE')
		layout.operator("object.add", text="Lattice",
						icon='OUTLINER_OB_LATTICE').type = 'LATTICE'
		layout.separator()
		layout.operator_menu_enum("object.empty_add", "type", text="Empty", icon='OUTLINER_OB_EMPTY')
		layout.separator()
		layout.operator("object.speaker_add", text="Speaker", icon='OUTLINER_OB_SPEAKER')
		layout.separator()
		layout.operator("object.camera_add", text="Camera",
						icon='OUTLINER_OB_CAMERA')
		layout.operator_menu_enum("object.lamp_add", "type",
								  icon="OUTLINER_OB_LAMP")
		layout.separator()

		layout.operator_menu_enum("object.effector_add", "type",
								  text="Force Field",
								  icon='OUTLINER_OB_EMPTY')
		layout.operator_menu_enum("object.group_instance_add", "group",
								  text="Group Instance",
								  icon='OUTLINER_OB_EMPTY')

class VIEW3D_BSI_transform_menu(bpy.types.Menu):
	bl_label = "Transform Menu"

	def draw(self, context):
		layout = self.layout
		MENU.object_reset_xform_menu(layout)
		layout.separator()
		MENU.object_freeze_xform_menu(layout)
		layout.separator()
		MENU.object_match_xform_menu(layout)
		layout.separator()
		layout.operator("bsi.object_pivot_to_selected", text="Move Center to Selected") 

class VIEW3D_MT_symmetry_menu(bpy.types.Menu):
	bl_label = "Mirror Menu"

	def draw(self, context):
		layout = self.layout
		MENU.symmetry_menu(layout)

class VIEW3D_MT_ParentMenu(bpy.types.Menu):
	bl_label = "Parent Menu"

	def draw(self, context):
		layout = self.layout
		MENU.parent_menu(layout)

class VIEW3D_MT_GroupMenu(bpy.types.Menu):
	bl_label = "Group Menu"

	def draw(self, context):
		layout = self.layout
		MENU.group_menu(layout)

class VIEW3D_MT_AlignMenu(bpy.types.Menu):
	bl_label = "Align Menu"

	def draw(self, context):
		layout = self.layout

		layout.menu("VIEW3D_MT_view_align_selected")
		layout.separator()

		layout.operator("view3d.view_all",
						text="Center Cursor and View All").center = True
		layout.operator("view3d.camera_to_view",
						text="Align Active Camera to View")
		layout.operator("view3d.view_selected")
		layout.operator("view3d.view_center_cursor")

class VIEW3D_PP_select_by_type_menu(bpy.types.Menu):
	bl_label = "Select By Type"
	def draw(self, context):
		layout = self.layout
		MENU.select_by_type(layout)

class VIEW3D_MT_select_menu(bpy.types.Menu):
	bl_label = "Select Menu"

	def draw(self, context):
		settings = context.tool_settings
		layout = self.layout
		layout.operator("bsi.tp_panel_selection", icon='MENU_PANEL')
		if context.mode == 'OBJECT':

			MENU.selection_object_mode(layout)
			# if context.active_object.type == 'LAMP':
		# 		pass
		# 	if context.active_object.type == 'CAMERA':
		# 		pass
		# 	else:
		# 		MENU.object_menu(layout)
		
		elif context.mode == 'EDIT_MESH':
			MENU.selection_mesh_edit_mode(layout)
		# 	sub_mod = settings.mesh_select_mode
		# 	MENU.selection_mesh_edit_mode(layout)
		# 	if sub_mod[0] == True: #Verts
		# 		MENU.vert_menu(layout)
		# 	if sub_mod[1] == True: #Edge
		# 		MENU.edge_menu(layout)
		# 	if sub_mod[2] == True: #Face
		# 		MENU.face_menu(layout)

<<<<<<< HEAD
		# if context.mode == 'EDIT_CURVE':
		# 	MENU.selection_curve_menu(layout)

		# if context.mode == 'EDIT_SURFACE':
		# 	MENU.surface_menu(layout)

		# if context.mode == 'EDIT_METABALL':
		# 	MENU.metaball_menu(layout)

		# elif context.mode == 'EDIT_LATTICE':
		# 	MENU.lattice_menu(layout)

		# if  context.mode == 'PARTICLE':
		# 	MENU.particle_menu(layout)

		# if context.mode == 'PAINT_WEIGHT':
		# 	MENU.weights_menu(layout)

		# elif context.mode == 'PAINT_VERTEX':
		# 	MENU.vcolor_menu(layout)

		# elif context.mode == 'PAINT_TEXTURE':
		# 	MENU.painttexture_menu(layout)

		# elif context.mode == 'SCULPT':
		# 	MENU.sclupt_menu(layout)

		# elif context.mode == 'EDIT_ARMATURE':
		# 	MENU.selection_armature_menu(layout)

		# if context.mode == 'POSE':
		# 	MENU.selection_pose_menu(layout)

=======
>>>>>>> origin/master
class VIEW3D_MT_SelectEditMenu(bpy.types.Menu):
	bl_label = "Select Menu"
	def draw(self, context):
		layout = self.layout
		MENU.selection_mesh_edit_mode(layout)

class VIEW3D_MT_SelectCurveMenu(bpy.types.Menu):
	bl_label = "Select Menu"
	def draw(self, context):
		layout = self.layout
		MENU.selection_curve_menu(layout)

class VIEW3D_MT_SelectArmatureMenu(bpy.types.Menu):
	bl_label = "Select Menu"
	def draw(self, context):
		layout = self.layout
		MENU.selection_armature_menu(layout)

class VIEW3D_MT_SelectPoseMenu(bpy.types.Menu):
	bl_label = "Select Menu"

	def draw(self, context):
		layout = self.layout
		MENU.selection_pose_menu(layout)

class VIEW3D_MT_PoseCopy(bpy.types.Menu):
	bl_label = "Pose Copy"

	def draw(self, context):
		layout = self.layout
		MENU.pose_copy(layout)
		layout.separator()

class VIEW3D_MT_PoseNames(bpy.types.Menu):
	bl_label = "Pose Copy"

	def draw(self, context):
		layout = self.layout

		layout.operator_context = 'EXEC_AREA' 
		layout.operator("pose.autoside_names",
						text="AutoName Left/Right").axis = 'XAXIS'
		layout.operator("pose.autoside_names",
						text="AutoName Front/Back").axis = 'YAXIS'
		layout.operator("pose.autoside_names",
						text="AutoName Top/Bottom").axis = 'ZAXIS'

		layout.operator("pose.flip_names")


class VIEW3D_MT_SelectSurface(bpy.types.Menu):
	bl_label = "Select Menu"

	def draw(self, context):
		layout = self.layout

		layout.operator("view3d.select_border")
		layout.operator("view3d.select_circle")

		layout.separator()

		layout.operator("curve.select_all").action = 'TOGGLE'
		layout.operator("curve.select_all").action = 'INVERT'
		layout.operator("curve.select_random")
		layout.operator("curve.select_nth")

		layout.separator()

		layout.operator("curve.select_row")

		layout.separator()

		layout.operator("curve.select_more")
		layout.operator("curve.select_less")

class VIEW3D_MT_SelectMetaball(bpy.types.Menu):
	bl_label = "Select Menu"

	def draw(self, context):
		layout = self.layout

		layout.operator("view3d.select_border")

		layout.separator()

		layout.operator("mball.select_all").action = 'TOGGLE'
		layout.operator("mball.select_all").action = 'INVERT'
		layout.operator("mball.select_random_metaelems")

class VIEW3D_MT_edit_TK(bpy.types.Menu):
	bl_label = "Edit Mesh Tools"

	def draw(self, context):
		layout = self.layout
		layout.row()  # XXX, is this needed?

		layout.operator_context = 'INVOKE_REGION_WIN'

		layout.menu("VIEW3D_MT_edit_mesh_vertices", icon='VERTEXSEL')
		layout.menu("VIEW3D_MT_edit_mesh_edges", icon='EDGESEL')
		layout.menu("VIEW3D_MT_edit_mesh_faces", icon='FACESEL')
		layout.separator()
		layout.menu("VIEW3D_MT_edit_mesh_normals", icon='META_DATA')
		layout.operator("mesh.loopcut_slide",
						text="Loopcut", icon='EDIT_VEC')



class VIEW3D_MT_edit_multi(bpy.types.Menu):
	bl_label = "Multi Select"

	def draw(self, context):
		layout = self.layout
		layout.operator_context = 'INVOKE_REGION_WIN'

		layout.separator()
		prop = layout.operator("wm.context_set_value", text="Vertex Select",
							   icon='VERTEXSEL')
		prop.value = "(True, False, False)"
		prop.data_path = "tool_settings.mesh_select_mode"

		prop = layout.operator("wm.context_set_value", text="Edge Select",
							   icon='EDGESEL')
		prop.value = "(False, True, False)"
		prop.data_path = "tool_settings.mesh_select_mode"

		prop = layout.operator("wm.context_set_value", text="Face Select",
							   icon='FACESEL')
		prop.value = "(False, False, True)"
		prop.data_path = "tool_settings.mesh_select_mode"
		layout.separator()

		prop = layout.operator("wm.context_set_value",
							   text="Vertex & Edge Select",
							   icon='EDITMODE_HLT')
		prop.value = "(True, True, False)"
		prop.data_path = "tool_settings.mesh_select_mode"

		prop = layout.operator("wm.context_set_value",
							   text="Vertex & Face Select",
							   icon='ORTHO')
		prop.value = "(True, False, True)"
		prop.data_path = "tool_settings.mesh_select_mode"

		prop = layout.operator("wm.context_set_value",
							   text="Edge & Face Select",
							   icon='SNAP_FACE')
		prop.value = "(False, True, True)"
		prop.data_path = "tool_settings.mesh_select_mode"
		layout.separator()

		prop = layout.operator("wm.context_set_value",
							   text="Vertex & Edge & Face Select",
							   icon='SNAP_VOLUME')
		prop.value = "(True, True, True)"
		prop.data_path = "tool_settings.mesh_select_mode"

class VIEW3D_MT_editM_Edge(bpy.types.Menu):
	bl_label = "Edges"

	def draw(self, context):
		layout = self.layout
		layout.operator_context = 'INVOKE_REGION_WIN'

		layout.operator("mesh.mark_seam")
		layout.operator("mesh.mark_seam", text="Clear Seam").clear = True
		layout.separator()

		layout.operator("mesh.mark_sharp")
		layout.operator("mesh.mark_sharp", text="Clear Sharp").clear = True
		layout.operator("mesh.extrude_move_along_normals", text="Extrude")
		layout.separator()

		layout.operator("mesh.edge_rotate",
						text="Rotate Edge CW").direction = 'CW'
		layout.operator("mesh.edge_rotate",
						text="Rotate Edge CCW").direction = 'CCW'
		layout.separator()

		layout.operator("TFM_OT_edge_slide", text="Edge Slide")
		layout.operator("mesh.loop_multi_select", text="Edge Loop")
		layout.operator("mesh.loop_multi_select", text="Edge Ring").ring = True
		layout.operator("mesh.loop_to_region")
		layout.operator("mesh.region_to_loop")


class VIEW3D_MT_BSI_curve_create(bpy.types.Menu):
	bl_label = "Curve"
	def draw(self, context):
		layout = self.layout
		MENU.curve_menu(layout)
		
class VIEW3D_MT_EditCurveCtrlpoints(bpy.types.Menu):
	bl_label = "Control Points"

	def draw(self, context):
		layout = self.layout

		edit_object = context.edit_object

		if edit_object.type == 'CURVE':
			layout.operator("transform.transform").mode = 'TILT'
			layout.operator("curve.tilt_clear")
			layout.operator("curve.separate")
			layout.separator()
			layout.operator_menu_enum("curve.handle_type_set", "type")
			layout.separator()
			layout.menu("VIEW3D_MT_hook")


class VIEW3D_MT_EditCurveSegments(bpy.types.Menu):
	bl_label = "Curve Segments"

	def draw(self, context):
		layout = self.layout

		layout.operator("curve.subdivide")
		layout.operator("curve.switch_direction")

class VIEW3D_MT_EditCurveSpecials(bpy.types.Menu):
	bl_label = "Specials"

	def draw(self, context):
		layout = self.layout

		layout.operator("curve.subdivide")
		layout.operator("curve.switch_direction")
		layout.operator("curve.spline_weight_set")
		layout.operator("curve.radius_set")
		layout.operator("curve.smooth")
		layout.operator("curve.smooth_weight")
		layout.operator("curve.smooth_radius")
		layout.operator("curve.smooth_tilt")

class VIEW3D_MT_EditArmatureTK(bpy.types.Menu):
	bl_label = "Armature Tools"

	def draw(self, context):
		layout = self.layout

		# Edit Armature

		layout.operator("transform.transform",
						text="Scale Envelope Distance").mode = 'BONE_SIZE'

		layout.operator("transform.transform",
						text="Scale B-Bone Width").mode = 'BONE_SIZE'
		layout.separator()

		layout.operator("armature.extrude_move")

		layout.operator("armature.extrude_forked")

		layout.operator("armature.duplicate_move")
		layout.operator("armature.merge")
		layout.operator("armature.fill")
		layout.operator("armature.delete")
		layout.operator("armature.separate")

		layout.separator()

		layout.operator("armature.subdivide", text="Subdivide")
		layout.operator("armature.switch_direction", text="Switch Direction")

class VIEW3D_MT_ArmatureName(bpy.types.Menu):
	bl_label = "Armature Name"

	def draw(self, context):
		layout = self.layout

		layout.operator_context = 'EXEC_AREA'
		layout.operator("armature.autoside_names",
						text="AutoName Left/Right").type = 'XAXIS'
		layout.operator("armature.autoside_names",
						text="AutoName Front/Back").type = 'YAXIS'
		layout.operator("armature.autoside_names",
						text="AutoName Top/Bottom").type = 'ZAXIS'
		layout.operator("armature.flip_names")
		layout.separator()

class VIEW3D_MT_KeyframeMenu(bpy.types.Menu):
	bl_label = "Keyframe Menu"

	def draw(self, context):
		layout = self.layout

		# Keyframe Block
		layout.operator("anim.keyframe_insert_menu",
						text="Insert Keyframe...")
		layout.operator("anim.keyframe_delete_v3d",
						text="Delete Keyframe...")
		layout.operator("anim.keying_set_active_set",
						text="Change Keying Set...")
		layout.separator()

# Classes for VIEW3D_MT_CursorMenu()
class VIEW3D_OT_pivot_cursor(bpy.types.Operator):
	"Cursor as Pivot Point"
	bl_idname = "view3d.pivot_cursor"
	bl_label = "Cursor as Pivot Point"

	@classmethod
	def poll(cls, context):
		return bpy.context.space_data.pivot_point != 'CURSOR'

	def execute(self, context):
		bpy.context.space_data.pivot_point = 'CURSOR'
		return {'FINISHED'}

class VIEW3D_OT_revert_pivot(bpy.types.Operator):
	"Revert Pivot Point"
	bl_idname = "view3d.revert_pivot"
	bl_label = "Reverts Pivot Point to median"

	@classmethod
	def poll(cls, context):
		return bpy.context.space_data.pivot_point != 'MEDIAN_POINT'

	def execute(self, context):
		bpy.context.space_data.pivot_point = 'MEDIAN_POINT'
		# @todo Change this to 'BOUDNING_BOX_CENTER' if needed...
		return{'FINISHED'}

class VIEW3D_MT_CursorMenu(bpy.types.Menu):
	bl_label = "Snap Cursor Menu"

	def draw(self, context):

		layout = self.layout
		layout.operator_context = 'INVOKE_REGION_WIN'
		layout.operator("view3d.snap_cursor_to_selected",
						text="Cursor to Selected")
		layout.operator("view3d.snap_cursor_to_center",
						text="Cursor to Center")
		layout.operator("view3d.snap_cursor_to_grid",
						text="Cursor to Grid")
		layout.operator("view3d.snap_cursor_to_active",
						text="Cursor to Active")
		layout.separator()
		layout.operator("view3d.snap_selected_to_cursor",
						text="Selection to Cursor")
		layout.operator("view3d.snap_selected_to_grid",
						text="Selection to Grid")
		layout.separator()
		layout.operator("view3d.pivot_cursor",
						text="Set Cursor as Pivot Point")
		layout.operator("view3d.revert_pivot",
						text="Revert Pivot Point")

class VIEW3D_MT_EditCursorMenu(bpy.types.Menu):
	bl_label = "Snap Cursor Menu"

	def draw(self, context):

		layout = self.layout
		layout.operator_context = 'INVOKE_REGION_WIN'
		layout.operator("view3d.snap_cursor_to_selected",
						text="Cursor to Selected")
		layout.operator("view3d.snap_cursor_to_center",
						text="Cursor to Center")
		layout.operator("view3d.snap_cursor_to_grid",
						text="Cursor to Grid")
		layout.operator("view3d.snap_cursor_to_active",
						text="Cursor to Active")
		layout.separator()
		layout.operator("view3d.snap_selected_to_cursor",
						text="Selection to Cursor")
		layout.operator("view3d.snap_selected_to_grid",
						text="Selection to Grid")
		layout.separator()
		layout.operator("view3d.pivot_cursor",
						text="Set Cursor as Pivot Point")
		layout.operator("view3d.revert_pivot",
						text="Revert Pivot Point")
		layout.operator("view3d.snap_cursor_to_edge_intersection",
						text="Cursor to Edge Intersection")
		layout.operator("transform.snap_type", text="Snap Tools",
						icon='SNAP_ON')

def abs(val):
	if val > 0:
		return val
	return -val

def edgeIntersect(context, operator):
	from mathutils.geometry import intersect_line_line

	obj = context.active_object

	if (obj.type != "MESH"):
		operator.report({'ERROR'}, "Object must be a mesh")
		return None

	edges = []
	mesh = obj.data
	verts = mesh.vertices

	is_editmode = (obj.mode == 'EDIT')
	if is_editmode:
		bpy.ops.object.mode_set(mode='OBJECT')

	for e in mesh.edges:
		if e.select:
			edges.append(e)

			if len(edges) > 2:
				break

	if is_editmode:
		bpy.ops.object.mode_set(mode='EDIT')

	if len(edges) != 2:
		operator.report({'ERROR'},
						"Operator requires exactly 2 edges to be selected")
		return

	line = intersect_line_line(verts[edges[0].vertices[0]].co,
							   verts[edges[0].vertices[1]].co,
							   verts[edges[1].vertices[0]].co,
							   verts[edges[1].vertices[1]].co)

	if line is None:
		operator.report({'ERROR'}, "Selected edges do not intersect")
		return

	point = line[0].lerp(line[1], 0.5)
	context.scene.cursor_location = obj.matrix_world * point

class VIEW3D_OT_CursorToEdgeIntersection(bpy.types.Operator):
	"Finds the mid-point of the shortest distance between two edges"

	bl_idname = "view3d.snap_cursor_to_edge_intersection"
	bl_label = "Cursor to Edge Intersection"

	@classmethod
	def poll(cls, context):
		obj = context.active_object
		return obj != None and obj.type == 'MESH'

	def execute(self, context):
		edgeIntersect(context, self)
		return {'FINISHED'}

class VIEW3D_MT_undoS(bpy.types.Menu):
	bl_label = "Undo/Redo"

	def draw(self, context):
		layout = self.layout
		layout.operator_context = 'INVOKE_REGION_WIN'
		layout.operator("ed.undo", icon='TRIA_LEFT')
		layout.operator("ed.redo", icon='TRIA_RIGHT')
###################################################################################
############################ PRIMITIVES MENU  #####################################
###################################################################################
class INFO_MT_bsi_primitive(Menu):
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	# bl_category = "AKT"
	bl_context = ""
	# bl_label = "AKT"
	bl_label = "BSI Primitive Menu"
	def draw(self, context):
		layout = self.layout

		layout.operator("object.empty_add", text="Null").type = 'PLAIN_AXES'
		layout.menu("INFO_MT_curve_add", text="Curve")
		layout.menu("INFO_MT_mesh_add", text="Polygon Mesh")
		layout.menu("INFO_MT_surface_add", text="Surface")
		layout.operator("object.add", text="Lattice").type = 'LATTICE'
		layout.operator("object.empty_add", text="Stand-in").type = 'CUBE'
		layout.operator_menu_enum("object.effector_add", "type", text="Control Object")
		layout.separator()
		layout.menu("INFO_MT_bsi_primitive_camera", text="Camera")
		layout.menu("INFO_MT_lamp_add", text="Light")
		layout.separator()
		layout.menu("INFO_MT_bsi_primitive_character", text="Model")
		layout.separator()
		layout.menu("INFO_MT_bsi_primitive_blender", text="Blender")

class INFO_MT_bsi_primitive_character(Menu):
	bl_label = "BSI Primitive Model"
	def draw(self, context):
		layout = self.layout
		layout.operator("wm.bsi_decorator_button", text="Skeleton - Man Basic") #TODO:
		layout.operator("wm.bsi_decorator_button", text="Skeleton - Man Complete")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Skeleton - Woman Basic")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Skeleton - Woman Complete")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Biped - Box")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Biped - Skeleton")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Quadruped - Rig")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Body - Male")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Body - Female")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Face - Male")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Face - Female")#TODO:

class INFO_MT_bsi_primitive_camera(Menu):
	bl_label = "BSI Primitive Camera"
	def draw(self, context):
		layout = self.layout
		layout.operator("object.camera_add", text="Perspective")
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Telephoto")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Wide Angle")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Orthographic")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Stereo")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Load...")#TODO:

class INFO_MT_bsi_primitive_blender(Menu):
	bl_label = "BSI Primitive Blender"
	def draw(self, context):
		layout = self.layout
		layout.operator("object.speaker_add", text="Speaker", icon='OUTLINER_OB_SPEAKER')
		layout.menu("INFO_MT_metaball_add", text="Metaball")
		layout.operator("object.text_add", text="Text", icon='OUTLINER_OB_FONT')

###################################################################################
############################# MATERIAL MENU  ######################################
###################################################################################
class INFO_MT_bsi_material(Menu):
	bl_label = "BSI Material Menu"
	def draw(self, context):
		layout = self.layout
		layout.operator("wm.bsi_decorator_button", text="Assing Material")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Remove Material")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Make Material Local")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Phong")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Lambert")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Blinn")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Cook Torrance")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Strauss")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Anisotropic")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Constant")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Ambient Occlusion")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Architectural")#TODO:
		layout.operator("wm.bsi_decorator_button", text="Car Paint")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Toon")#TODO:
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Load...")#TODO:

###################################################################################
############################# PROPERTY MENU  ######################################
###################################################################################
class INFO_MT_bsi_property(Menu):
	bl_label = "BSI Property Menu"
	def draw(self, context):
		layout = self.layout
		layout.menu("INFO_MT_bsi_texture_projection", text="Texture Map")
		layout.separator()
		layout.operator("object.vertex_group_add", text="Vert Color Map")
		layout.operator("wm.bsi_decorator_button", text="Render Map (N/A)") #no need
		layout.operator("wm.bsi_decorator_button", text="Symmetry Map (N/A)")
		layout.operator("wm.bsi_decorator_button", text="User Data Map (N/A)")  #User data are custom parameter values that are attached directly to components
		layout.operator("wm.bsi_decorator_button", text="Weight Map (N/A)") #weight map module
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="GATOR (N/A)") # Motion capture retargeting module
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Annotation (TODO)")
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Attribute Display (N/A)")
		layout.menu("INFO_MT_bsi_display", text="Display (TODO)") 
		layout.operator("wm.bsi_decorator_button", text="Polygon Cluster Visibility (N/A)") 
		layout.menu("INFO_MT_bsi_geometry_approx", text="Geometry Approximation") 
		layout.operator("wm.bsi_decorator_button", text="Static Kinestate (N/A)") 
		layout.operator("wm.bsi_decorator_button", text="Synoptic (N/A)") 
		layout.operator("wm.bsi_decorator_button", text="Transfomr Setup (N/A)") 
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Ultimapper (TODO)") 
		layout.operator("wm.bsi_decorator_button", text="Binormal (TODO)") 
		layout.operator("wm.bsi_decorator_button", text="Tangent (N/A)") 
		layout.operator("wm.bsi_decorator_button", text="Custom Color (N/A)") 
		layout.operator("wm.bsi_decorator_button", text="Text Prop (Same as Annotoation)") 
		#don't know if overrides exist in blender. used for render pass as such
		layout.operator("wm.bsi_decorator_button", text="Override Tools (N/A") 

class INFO_MT_bsi_texture_projection(Menu):
	bl_label = "BSI Texture Projection"
	def draw(self, context):
		layout = self.layout
		layout.operator("wm.bsi_decorator_button", text="Planar XY TODO")
		layout.operator("wm.bsi_decorator_button", text="Planar YZ TODO")
		layout.operator("wm.bsi_decorator_button", text="Planar XZ TODO")
		layout.operator("uv.cylinder_project", text="Cylindrical")
		layout.operator("uv.sphere_project", text="Spherical")
		layout.operator("wm.bsi_decorator_button", text="Spatial (N/A)")
		layout.operator("uv.cube_project", text="Cubic")
		layout.operator("uv.project_from_view", text="Camera Projection").scale_to_bounds = False
		layout.operator("uv.project_from_view", text="Project from View (Bounds)").scale_to_bounds = True
		layout.operator("wm.bsi_decorator_button", text="Unique UVs (polymesh)")
		layout.operator("wm.bsi_decorator_button", text="Contour Stretch UV")
		layout.separator()
		layout.operator_context = 'EXEC_REGION_WIN'
		layout.operator("uv.cube_project")
		layout.operator("uv.cylinder_project")
		layout.operator("uv.sphere_project")
		layout.separator()
		layout.operator("uv.unwrap", text="Unfold")
		layout.separator()
		layout.operator("uv.reset")

class INFO_MT_bsi_display(Menu):
	bl_label = "BSI Display"
	def draw(self, context):
		layout = self.layout

class INFO_MT_bsi_geometry_approx(Menu):
	bl_label = "BSI Geometry Approximation"
	def draw(self, context):
		layout = self.layout
		layout.operator("object.shade_smooth", text="Smooth") #weight map module
		layout.operator("object.shade_flat", text="Flat") #weight map module

###################################################################################
############################### -------- ##########################################
###################################################################################
class INFO_MT_bsi_object(Menu):
	bl_label = "BSI Object Menu"
	def draw(self, context):
		layout = self.layout
		bsi_menu_lib.transforms_menu(layout)
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Uniform (N/A)") 
		layout.operator("wm.bsi_decorator_button", text="Vol (N/A)") 
		layout.separator()
		bsi_menu_lib.misc_menu(layout)
		# layout.operator("wm.bsi_decorator_button", text="Sym (N/A)") 

class INFO_MT_bsi_edge(Menu):
	bl_label = "BSI Object Edge"
	def draw(self, context):
		layout = self.layout
		# layout.operator("wm.bsi_decorator_button", text="") 
		layout.operator("bpy.ops.bsi.dlg_rename", text="Rename") 
		layout.operator("bsi.mesh_delete", text="Delete") 
		layout.separator()
		bsi_menu_lib.selection_menu(layout)
		layout.separator()
		bsi_menu_lib.duplicate_menu(layout)
		layout.separator()
		bsi_menu_lib.group_menu(layout)
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Info Selection") 
		layout.operator("wm.bsi_decorator_button", text="Explore") 
		layout.operator("wm.bsi_decorator_button", text="Open Attachements") 
		layout.operator("wm.bsi_decorator_button", text="Inspect Material") 
		layout.operator("wm.bsi_decorator_button", text="Last Operator Stack") 
		layout.operator("wm.bsi_decorator_button", text="Properties") 
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Select Constraining Objects") 
		layout.operator("wm.bsi_decorator_button", text="Select Objects Constrained") 
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Copy All Animation")
		layout.operator("wm.bsi_decorator_button", text="Paste All Animation")
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Set Thumbnail (N/A)")
		layout.operator("wm.bsi_decorator_button", text="St Thumbnail From Region(N/A)")
		layout.operator("wm.bsi_decorator_button", text="Clear Thumbnail(N/A)")
		layout.separator()
		layout.operator("wm.bsi_decorator_button", text="Set User Keywords")
		layout.separator()
		bsi_menu_lib.edge_tools_menu(layout)
		layout.separator()
		bsi_menu_lib.misc_menu(layout)

class INFO_MT_bsi_view(Menu):
	bl_label = "BSI View Edge"
	def draw(self, context):
		layout = self.layout
		bsi_menu_lib.transforms_menu(layout)
		layout.separator()

###################################################################################
################################ CAMERA VIEW ######################################
###################################################################################
class INFO_MT_bsi_camera_view(Menu):
	bl_label = "Camera View"
	def draw(self, context):
		layout = self.layout
		layout.operator("wm.bsi_decorator_button", text="Undo (N/A)") 
		layout.operator("wm.bsi_decorator_button", text="Redo (N/A)") 
		layout.separator()
		# OpenGL render
		layout.operator("render.opengl", text="OGL Render Still", icon='RENDER_STILL')
		layout.operator("render.opengl", text="OGL Render Animation", icon='RENDER_ANIMATION').animation = True
		layout.operator("view3d.object_as_camera", text="Select Camera") 
		layout.operator("wm.bsi_decorator_button", text="Select Interest") 
		# layout.operator("bsi.dlg_camera_property", text="Properties...") 
		layout.operator("bsi.view3d_dynamic_prop_panel",text="Properties...").panel="viewport_properties_panel"
		layout.separator()
		layout.operator("object.camera_add", text="Camera")
		# layout.operator("wm.bsi_decorator_button", text="New Camera") 
		layout.separator()
		# layout.operator("wm.bsi_decorator_button", text="Start Capture(N/A)") 
		# layout.separator()
		layout.operator("view3d.localview", text="Isolate Selection") 
		# layout.operator("wm.bsi_decorator_button", text="Update Isolated(N/A)") 
		# layout.operator("wm.bsi_decorator_button", text="Add Selected (N/A)") 
		# layout.operator("wm.bsi_decorator_button", text="Remove Selected(N/A)") 
		layout.separator()
		layout.operator("view3d.view_selected", text="Frame Selection") 
		layout.operator("view3d.view_center_lock", text="Center Selection") 
		layout.operator("view3d.view_all", text="Frame All")#.type='CENTER'
		layout.operator("view3d.view_center_camera", text="Reset TODO") 
		layout.separator()
		
		layout.operator("bsi.view3d_set_background_color", text="Set Background to Black").color = (0.0,0.0,0.0)
		layout.operator("bsi.view3d_set_background_color", text="Set Background to Grey").color = (0.45,0.45,0.45)
		layout.operator("wm.bsi_decorator_button", text="Scene Colors TODO") 
		layout.separator()
		# layout.menu("VIEW3D_MT_view_navigation", text="Camera Tools")
		# layout.operator("wm.bsi_decorator_button", text="Navigation Tool (N/A)") 
		# layout.operator("wm.bsi_decorator_button", text="Pan & Zoom Tool (N/A)") 
		layout.operator("view3d.zoom_border", text="Rectangular Zoom Tool") 
		layout.operator("view3d.view_orbit", text="Orbit Tool")
		# layout.operator("wm.bsi_decorator_button", text="Dolly Tool(N/A)") 
		# layout.operator("wm.bsi_decorator_button", text="Track Tool(N/A)") 
		layout.operator("view3d.view_roll", text="Roll Tool").type='ROLLANGLE'
		# layout.operator("view3d.zoom_border", text="Zoom Tool") 
		# layout.operator("wm.bsi_decorator_button", text="Pivot Tool (N/A)") 
		layout.operator("view3d.navigate", text="Fly Tool") 
		# layout.operator("wm.bsi_decorator_button", text="Walk Tool (N/A)") 
		# layout.operator("wm.bsi_decorator_button", text="Drive Tool (N/A)") 
		# layout.separator()
		# layout.operator("wm.bsi_decorator_button", text="Mute (N/A)") 
		# layout.operator("wm.bsi_decorator_button", text="Solo (N/A)") 

###################################################################################
################################# 3D VIEW #########################################
###################################################################################
class INFO_VIEW3D_bsi_display(Menu):
	bl_label = "View3d Drawing"
	def draw(self, context):
		layout = self.layout
		v3d = context.space_data
		scene 	= context.scene
		cursor_set = GUI.get_view3d_cursor()
		obj = context.object
		display_all = v3d.show_only_render
		
		# layout.prop(cursor_set, "cursor_visible", text="Toggle Cursor", toggle=True,
		# 		 icon=('RESTRICT_VIEW_OFF' if cursor_set.cursor_visible
		# 			   else 'RESTRICT_VIEW_ON'))

		# layout.operator("wm.bsi_decorator_button", text="Fast manipulation") 
		# layout.operator("wm.bsi_decorator_button", text="Fast Playback(2D|3D Wireframe)") 
		# layout.operator("wm.bsi_decorator_button", text="Mixer Ghosting") 
		# layout.operator("wm.bsi_decorator_button", text="Animation Ghosting") 
		# layout.operator("wm.bsi_decorator_button", text="Skeleton FK/IK Ghosting") 
		layout.operator("bsi.view3d_dynamic_prop_panel",text="Properties...").panel="viewport_properties_panel"
		
		layout.separator()
		layout.prop(context.scene, "boundbox", text="Bounding Box")
		layout.prop(context.scene, "wireframe", text="Wireframe")
		layout.prop(context.scene, "constant", text="Constant")
		layout.prop(context.scene, "solid", text="Shaded")
		layout.prop(context.scene, "textured", text="Textured")
		layout.prop(context.scene, "textured_decal", text="Textured Decal")
		
		layout.separator()
		
		if not display_all:
			layout.prop(context.scene, "show_wire", text="Wireframe On Shaded")
			if v3d.viewport_shade not in {'BOUNDBOX', 'WIREFRAME'}:
				if obj and obj.mode == 'EDIT':
					layout.prop(context.scene, "occlude_wire", text="Occlude Wire")
					layout.prop(v3d, "show_occlude_wire")
			layout.prop(context.scene, "show_x_ray", text="X Ray <--LAME")
			layout.prop(context.scene, "show_all_edges", text="All Edges <--WTF is this")
			layout.prop(context.scene, "show_axis", text="Axis")
			layout.prop(context.scene, "show_bounds", text="Bounding Boxes")
			layout.prop(context.scene, "show_name", text="Names")
			layout.prop(context.scene, "show_only_shape_key", text="Shapes")
			layout.prop(v3d, "show_backface_culling")
			
			if v3d.show_backface_culling:
				layout.prop(context.scene, "show_transparent", text="Hide Outline")
			layout.prop(context.scene, "show_texture_space", text="Texture Space")
			layout.prop(v3d, "show_outline_selected")
			layout.prop(v3d, "show_all_objects_origin", text = "Centers")
			layout.prop(v3d, "show_relationship_lines", text = "Relationships")
		

		
		# layout.operator("wm.bsi_decorator_button", text="Construction Mode Display (MENU)? ") 
		# layout.operator("wm.bsi_decorator_button", text="Headlight (N/A)") 
		# layout.operator("wm.bsi_decorator_button", text="Wireframe On Shaded") 
		# layout.operator("wm.bsi_decorator_button", text="XRay")
		# layout.separator() 
		# layout.operator("bsi.view3d_shading", text="Bound Box").types = "BOUNDBOX"
		# layout.operator("bsi.view3d_shading", text="Wireframe").types = "WIREFRAME"
		# layout.operator("bsi.bsi_decorator_button", text="Depth Cue")
		# layout.operator("wm.bsi_decorator_button", text="Constant") 
		# layout.operator("bsi.view3d_shading", text="Solid").types = "SOLID"
		# layout.operator("bsi.view3d_shading", text="Textured").types = "TEXTURED"
		# layout.operator("wm.bsi_decorator_button", text="Texture Decal") 
		# layout.operator("bsi.view3d_shading", text="Rendered").types = "RENDERED"
		# layout.operator("wm.bsi_decorator_button", text="Realtime shaders") 
		# layout.operator("wm.bsi_decorator_button", text="Rotoscope(N/A)")
		# layout.operator("wm.bsi_decorator_button", text="Display Options(N/A)")
		# layout.operator("wm.bsi_decorator_button", text="Rotoscopy Options (N/A)")   

		# scene = context.scene
		# gs = scene.game_settings
		# obj = context.object

		# col = layout.column()
		# col.prop(view, "show_textured_solid")
		# col.prop(view, "use_matcap")
		# col.prop(view, "show_textured_shadeless")
		# col.prop(view, "show_backface_culling")
		# col.prop(view, "show_occlude_wire")
		# # if not scene.render.use_shading_nodes:
		# #     col.prop(gs, "material_mode", text="")

		# # if view.viewport_shade == 'SOLID':
		# #     col.prop(view, "show_textured_solid")
		# #     col.prop(view, "use_matcap")
		# #     if view.use_matcap:
		# #         col.template_icon_view(view, "matcap_icon")
		# # elif view.viewport_shade == 'TEXTURED':
		#     # if scene.render.use_shading_nodes or gs.material_mode != 'GLSL':
		#         col.prop(view, "show_textured_shadeless")

		# col.prop(view, "show_backface_culling")

		# if view.viewport_shade not in {'BOUNDBOX', 'WIREFRAME'}:
		#     if obj and obj.mode == 'EDIT':
		#         col.prop(view, "show_occlude_wire")

		# fx_settings = view.fx_settings

		# if view.viewport_shade not in {'BOUNDBOX', 'WIREFRAME'}:
		#     sub = col.column()
		#     sub.active = view.region_3d.view_perspective == 'CAMERA'
		#     sub.prop(fx_settings, "use_dof")
		#     col.prop(fx_settings, "use_ssao", text="Ambient Occlusion")
		#     if fx_settings.use_ssao:
		#         ssao_settings = fx_settings.ssao
		#         subcol = col.column(align=True)
		#         subcol.prop(ssao_settings, "factor")
		#         subcol.prop(ssao_settings, "distance_max")
		#         subcol.prop(ssao_settings, "attenuation")
		#         subcol.prop(ssao_settings, "samples")
		#         subcol.prop(ssao_settings, "color")
		# layout.operator("game_settings.material_mode", text ="asdf").type = 'GLSL'

class INFO_VIEW3D_bsi_view(Menu):
	bl_label = "View3d Display"
	def draw(self, context):
		layout = self.layout

		# layout.operator("wm.bsi_decorator_button", text="Manipulate Tool (N/A)")
		layout.operator("bsi.view3d_toggle_grid", text="Toggle Grid")
		layout.operator("wm.bsi_decorator_button", text="Frame Rate (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Selection Info (N/A)")
		layout.separator()       
		# ['EMPTY','LAMP','CAMERA','SPEAKER','LATTICE','ARMATURE','FONT','META','SURFACE','CURVE','MESH']
		
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Lights").node = "LAMP"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Cameras").node = "CAMERA"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Meshes").node = "MESH"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Surfaces").node = "SURFACE"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Curves").node = "CURVE"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Nulls").node = "EMPTY"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Speakers").node = "SPEAKER"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Fonts").node = "FONT"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Armatures").node = "ARMATURE"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Lattices").node = "LATTICE"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Metalballs").node = "META"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Force").node = "FORCE"

		# layout.operator("bsi.view3d_node_visibilty_toggle", text="Hair (N/A)").node = ""
		# layout.operator("bsi.view3d_node_visibilty_toggle", text="Transform Groups (N/A)").node = ""
		# layout.operator("bsi.view3d_node_visibilty_toggle", text="Texture Controls (N/A)").node = ""
		# layout.operator("bsi.view3d_node_visibilty_toggle", text="Control Objects (N/A)").node = ""
		# layout.operator("bsi.view3d_node_visibilty_toggle", text="Instances (N/A)").node = ""
		layout.separator()       
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Show All").node = "SHOW_ALL"
		layout.operator("bsi.view3d_node_visibilty_toggle", text="Hide All").node = "HIDE_ALL"
		layout.separator()       
		layout.operator("wm.bsi_decorator_button", text="Invisible Components (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Name (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Attachemnts (Annotation and Synoptic) (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Unselected Clusters(except Sample) (N/A)")
		layout.operator("wm.bsi_decorator_button", text="PolyMesh Boundaries and Hard Edges (N/A)")
		layout.operator("wm.bsi_decorator_button", text="PolyMesh Hulls (N/A)")
		layout.operator("wm.bsi_decorator_button", text="PolyMesh Internal Edges (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Subdivision Surfaces (N/A)")
		layout.operator("wm.bsi_decorator_button", text="NURB Boundaries (N/A)")
		layout.operator("wm.bsi_decorator_button", text="NURB Hulls (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Points (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Knots (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Sampled Points (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Centers (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Normals (N/A)")
		layout.operator("wm.bsi_decorator_button", text="PolyNormals (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Cones (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Relations (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Animation Cues (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Weight Maps (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Weight Points (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Unselected Tag Pints (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Distance to Output Camera (N/A)")
		layout.operator("wm.bsi_decorator_button", text="X-Ray (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Show All (N/A)")
		layout.separator()       
		layout.operator("wm.bsi_decorator_button", text="Hide All (N/A)")
		layout.operator("wm.bsi_decorator_button", text="Hide All (N/A)")
		layout.separator()       
		layout.operator("wm.bsi_decorator_button", text="Visibilty (N/A)")



def register():
	bpy.utils.register_module(__name__)

	# wm = bpy.context.window_manager
	# kc = wm.keyconfigs.addon
	# if kc:
	#     km = kc.keymaps.new(name='3D View', space_type='VIEW_3D')
	#     kmi = km.keymap_items.new('wm.call_menu', 'SPACE', 'PRESS')
	#     kmi.properties.name = "ImageBlender Context Menu
def unregister():
	bpy.utils.unregister_module(__name__)

	# wm = bpy.context.window_manager
	# kc = wm.keyconfigs.addon
	# if kc:
	#     km = kc.keymaps['3D View']
	#     for kmi in km.keymap_items:
	#         if kmi.idname == 'wm.call_menu':
	#             if kmi.properties.name == "IB_ContextMenu":
	#                 km.keymap_items.remove(ImageBlender Context Menu                    break

if __name__ == "__main__":
	register()
