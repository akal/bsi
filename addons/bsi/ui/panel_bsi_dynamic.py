import bpy
from bpy.props import *
# from ..libs import gui_utilities
from ..ui import panel_layouts as PANEL

############################### CONSTANTS ###############################
# GUI = gui_utilities.GuiUtilities()
############################### CONSTANTS ###############################

#dynamic_menu_id = 'None'

class BSI_dynamic_bsi_panel(bpy.types.Panel):
	bl_label = " "
	bl_idname = "BSI_dynamic_bsi_panel"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	bl_category = "BSI"

	panel = "Model"

	def draw_header(self, context):
		layout = self.layout
		layout.operator_menu_enum("bsi.view3d_bsi_tool_panel", "panel_choices", "%s" % self.panel )#Here the operator menu is displayed
	
	def draw(self, context):
		layout = self.layout
		dyn_menu = "%s(layout, context)" % self.panel
		exec (dyn_menu)

class VIEW3D_OT_dynamic_bsi_panel_op(bpy.types.Operator):#When an option in the operator menu is clicked, this is called
	"""BSI dynamic tools panel exec"""
	bl_idname = "bsi.view3d_bsi_tool_panel"
	bl_label = ""
	bl_options = {'REGISTER'}
	enums = (
					("0", "Model", "The first item"),
					("1", "Animate", "The second item"),
					("2", "Render", "The third item"),
					("3", "Simulate", "The forth item"),
					("4", "Hair", "The fifth item"),
				)

	#Define possible operations
	panel_choices = EnumProperty(items= enums)

	def execute(self, context):
		dynamic_menu_id = self.properties.panel_choices#Store the choosen operation
		dyn = bpy.types.BSI_dynamic_bsi_panel
		bpy.utils.unregister_class(dyn)

		if dynamic_menu_id == "0":
			ct = context.user_preferences.themes.items()[0][0] 
			ui = context.user_preferences.themes[ct].user_interface
			ui.wcol_tool.inner = (0.62,0.54,0.64,1)
			dyn.panel = "Model"

		if dynamic_menu_id == "1":
			ct = context.user_preferences.themes.items()[0][0] 
			ui = context.user_preferences.themes[ct].user_interface
			ui.wcol_tool.inner = (0.5,0.65,0.55,1)
			dyn.panel = "Animate"

		if dynamic_menu_id == "2":
			ct = context.user_preferences.themes.items()[0][0] 
			ui = context.user_preferences.themes[ct].user_interface
			ui.wcol_tool.inner = (0.55,0.6,0.63,1)
			dyn.panel = "Render"

		if dynamic_menu_id == "3":
			ct = context.user_preferences.themes.items()[0][0] 
			ui = context.user_preferences.themes[ct].user_interface
			ui.wcol_tool.inner = (0.72,0.53,0.53,1)
			dyn.panel = "Simulate"

		if dynamic_menu_id == "4":
			ct = context.user_preferences.themes.items()[0][0] 
			ui = context.user_preferences.themes[ct].user_interface
			ui.wcol_tool.inner = (0.72,0.65,0.65,1)
			dyn.panel = "Hair"

		bpy.utils.register_class(dyn)

		return {'FINISHED'}
	
	
def Model(layout, context):

	col = layout.column(align=True)
	col.operator("wm.bsi_decorator_button",text="Get")
	col.menu("INFO_MT_bsi_primitive", text="Primitive")
	col.menu("INFO_MT_bsi_material", text="Material")
	col.menu("INFO_MT_bsi_property", text="Property")
	
	col = layout.column(align=True)
	col.operator("wm.bsi_decorator_button",text="Create")
	col.menu("VIEW3D_MT_BSI_curve_create", text="Curve")
	col.menu("INFO_MT_add", text="Surf.Mesh")
	col.menu("INFO_MT_add", text="Poly.Mesh")
	col.menu("INFO_MT_add", text="Skeleton")
	col.menu("INFO_MT_add", text="Model")
	col.menu("INFO_MT_add", text="Text")

	col = layout.column(align=True)
	col.operator("wm.bsi_decorator_button",text="Modify")
	col.menu("INFO_MT_add", text="Curve")
	col.menu("INFO_MT_add", text="Surf.Mesh")
	col.menu("INFO_MT_add", text="Poly.Mesh")
	col.menu("INFO_MT_add", text="Deform")
	col.menu("INFO_MT_add", text="Model")

def Animate(layout, context):
	col = layout.column(align=True)
	col.operator("wm.bsi_decorator_button",text="Get")
	col.menu("INFO_MT_bsi_primitive", text="Primitive")
	col.menu("INFO_MT_bsi_material", text="Material")
	col.menu("INFO_MT_bsi_property", text="Property")

def Simulate(layout, context):
	col = layout.column(align=True)
	col.operator("wm.bsi_decorator_button",text="Get")
	col.menu("INFO_MT_bsi_primitive", text="Primitive")
	col.menu("INFO_MT_bsi_material", text="Material")
	col.menu("INFO_MT_bsi_property", text="Property")

def Render(layout, context):
	col = layout.column(align=True)
	col.operator("wm.bsi_decorator_button",text="Get")
	col.menu("INFO_MT_bsi_primitive", text="Primitive")
	col.menu("INFO_MT_bsi_material", text="Material")
	col.menu("INFO_MT_bsi_property", text="Property")

def Hair(layout, context):
	col = layout.column(align=True)
	col.operator("wm.bsi_decorator_button",text="Get")
	col.menu("INFO_MT_bsi_primitive", text="Primitive")
	col.menu("INFO_MT_bsi_material", text="Material")
	col.menu("INFO_MT_bsi_property", text="Property")
	
# def register():
# 	bpy.utils.register_module(__name__)

# def unregister():
# 	bpy.utils.unregister_module(__name__)


# if __name__ == "__main__":
#     register()
