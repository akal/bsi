
import bpy
from bpy.types import Operator


class BSI_RenameDialog(Operator):
    """BSI rename operator"""
    bl_idname = "bsi.dlg_rename_op"
    bl_label = "Rename Dialog Op"
    bl_options = {'REGISTER','UNDO'}

    def execute(self, context):
        bpy.ops.bsi.dlg_rename('INVOKE_DEFAULT')
        return {'FINISHED'}

class RenameDialog(Operator):
    bl_idname = "bsi.dlg_rename"
    bl_label = "Rename Dialog"


    string = bpy.props.StringProperty(name="New Name")
    
    @classmethod
    def poll(cls, context):
        return context.selected_objects != None

    def execute(self, context):
        name = self.string
        import re
        new_name = re.sub('[^\w\-_\. ]', '', name)
        for i in range(10,1,-1):
            new_name = new_name.replace((" "*i), "")

        new_name = new_name.replace(" ", "_")
        if new_name[-1] =="_":
            new_name = new_name[:-1]
        obs = context.selected_editable_objects

        for ob in obs:
            ob.name = new_name
            ob.data.name = new_name

        self.report({'INFO'}, new_name)
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

class CameraPropertyDialog(Operator):
    bl_idname = "bsi.dlg_camera_property"
    bl_label = "Camera Property"

    lens_f = bpy.props.FloatProperty(name="Lens")
    clip_start_f = bpy.props.FloatProperty(name="Start")
    clip_end_f = bpy.props.FloatProperty(name="End")
    
    @classmethod
    def poll(cls, context):
        return context.selected_objects != None

    def execute(self, context):
        context.space_data.lens = lens
        context.space_data.clip_start = start_c
        context.space_data.clip_end = end_c
        return {'FINISHED'}
    
    def invoke(self, context, event):
        global lens, start_c, end_c
        lens                = context.space_data.lens
        start_c             = context.space_data.clip_start
        end_c               = context.space_data.clip_end

        self.lens_f         = lens 
        self.clip_start_f   = start_c
        self.clip_end_f     = end_c

        wm = context.window_manager
        return wm.invoke_props_dialog(self)