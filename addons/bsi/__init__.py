# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "BSI Tools",
	"author": "Anisim Kalugin",
	"version": (0, 3, 0),
	"blender": (2, 74, 5),
	"location": "View3D",
	"description": "Blender|Softimage workflow",
	"warning": "",
	"wiki_url": "",
	"category": "BSI",
}

if "bpy" in locals():
	import importlib
	
	importlib.reload(ops.armature)
	importlib.reload(ops.gui)
	importlib.reload(ops.material)
	importlib.reload(ops.mesh)
	importlib.reload(ops.object)
	importlib.reload(ops.text)
	importlib.reload(ops.uv)
	importlib.reload(ops.view3d)

	importlib.reload(ui.dialogs)
	importlib.reload(ui.menus)
	importlib.reload(ui.panels)
	importlib.reload(ui.ui)
	importlib.reload(ui.panel_bsi_dynamic)
	importlib.reload(ui.panel_prop_dynamic)
	importlib.reload(ui.panel_tool_dynamic)
	importlib.reload(ui.uv_panels)
else:
	from .ops import armature
	from .ops import gui
	from .ops import material
	from .ops import mesh
	from .ops import object
	from .ops import text
	from .ops import uv
	from .ops import view3d
	
	from .ui import dialogs
	from .ui import menus
	from .ui import panels
	from .ui import ui
	from .ui import panel_bsi_dynamic
	from .ui import panel_prop_dynamic
	from .ui import panel_tool_dynamic
	from .ui import uv_panels

import bpy, os
from .libs import properties as prop
from .libs import gui_utilities

def register():
	bpy.utils.register_module(__name__)
	bpy.types.Material.BSI = bpy.props.PointerProperty(type=prop.BSI_Properties)
	gui_utilities.GuiUtilities().unregister_property_panels()

def unregister():
	bpy.utils.unregister_module(__name__)
	del bpy.types.Material.BSI
	gui_utilities.del_icons()

if __name__ == "__main__":
	register()

