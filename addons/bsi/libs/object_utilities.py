import bpy
from bpy import context


class ObjectUtilities(object):
	def __non_editable_nodes(self):
		r"""
			returns a list of editable objects
			font is editable, but can't get out of it via space bar so 
			users will have to edit it manually
		"""
		return ['EMPTY','CAMERA','LAMP','SPEAKER','FONT'] 
	
	def is_editable(self, ob):
		if ob.type in self.__non_editable_nodes():
			return False
		return True

	def get_object_type(self, object_node):
		r"""
			Have to do this because particle nodes are simply nulls 
			(blender inconsistency)
		"""
		if object_node.type == "EMPTY" and object_node.field.type != None:
			return 'FIELD'
		return object_node.type

	def select_children(self, object_node):
		children=list(object_node.children)
		bpy.ops.object.select_all(action='DESELECT')
		for each in children:
			each.select = True
			for child in each.children:
				children.append(child)
		object_node.select = True
		bpy.context.scene.objects.active = object_node
		return True

	def select_tree(self, object_node):
		bpy.ops.object.select_all(action='DESELECT')
		if object_node.parent != None:
			parent = True
			while parent:
				if object_node.parent != None:
					object_node = object_node.parent
				else:
					parent = False

		# bpy.ops.object.select_all(action='DESELECT')
		object_node.select = True
		bpy.context.scene.objects.active = object_node
		self.select_children(object_node)