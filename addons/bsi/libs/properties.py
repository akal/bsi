import bpy
from bpy.types import PropertyGroup
from bpy.props import FloatVectorProperty, BoolProperty, StringProperty
from . import gui_utilities
from . import mesh_utilities
############################### CONSTANTS ###############################
GUI = gui_utilities.GuiUtilities()
MESH = mesh_utilities.MeshUtilities()
############################### CONSTANTS ###############################

##########################################################
#viewport callbacks
##########################################################

def viewport_display_callback(scene, prop):
	r""" A callback for viewport related drawing logic"""
	# print("Property '%s' of %s updated to value %s" % (prop, scene.name, getattr(scene, prop)))
	scene_props = ["boundbox", "wireframe", "constant", "solid", "textured", "textured_decal"]

	if prop not in scene_props:
		return
	scene_props.pop(scene_props.index(prop))

	if getattr(scene, prop):
		for prp in scene_props:
			setattr(scene, prp, False)

	if prop == "textured_decal" or prop == "textured":
		MESH.set_objects_textured(draw_texture = True)
	else:
		MESH.set_objects_textured(draw_texture = False)

	v3d = GUI.get_screen_area(	"VIEW_3D")
	if prop == "textured_decal":
		v3d.viewport_shade = 'TEXTURED'
		if v3d.show_textured_solid == True:
			v3d.show_textured_solid = False
			v3d.show_textured_shadeless = True

	elif prop == "constant":
		v3d.viewport_shade = 'TEXTURED'
		if v3d.show_textured_solid == True:
			v3d.show_textured_solid = False
			v3d.show_textured_shadeless = False

	else:
		if v3d.show_textured_solid != True:
			v3d.show_textured_solid = False
			v3d.show_textured_shadeless = True
		if prop == "textured":
			v3d.show_textured_solid = True
			v3d.show_textured_shadeless = False
		v3d.viewport_shade = '%s' % prop.upper()

def bsi_viewport_display(prop):
	def update(self, context):
		scene = context.scene
		viewport_display_callback(self, prop)
	return update

def object_display_callback(scene, prop):
	print("Property '%s' of %s updated to value %s" % (prop, scene.name, getattr(scene, prop)))

	v3d = GUI.get_screen_area("VIEW_3D")

	if prop == "occlude_wire":
		attrs = ['use_occlude_geometry']
		for attr, toggle in map(lambda attr:(attr, getattr(v3d, attr)), attrs):
			setattr(v3d, attr, not toggle)
	else:
		GUI.toggle_object_visibilities(prop)

def bsi_object_display(prop):
	def update(self, context):
		object_display_callback(self, prop)
	return update

##########################################################
#Scaling and toggles controls
##########################################################
def bsi_scale_toggle(self, context):
	view3d = GUI.get_screen_area("VIEW_3D")
	if context.scene.bsi_scale == True:
		context.scene.bsi_rotate = False
		context.scene.bsi_translate = False

		if view3d.show_manipulator == False:
			view3d.show_manipulator = True
			view3d.transform_manipulators = {'SCALE'}

	else:
		view3d.show_manipulator = False

def bsi_rotate_toggle(self, context):
	view3d = GUI.get_screen_area("VIEW_3D")
	if context.scene.bsi_rotate == True:
		context.scene.bsi_scale = False
		context.scene.bsi_translate = False
		if view3d.show_manipulator == False:
			view3d.show_manipulator = True
			view3d.transform_manipulators = {'ROTATE'}

	else:
		view3d.show_manipulator = False

def bsi_translate_toggle(self, context):
	view3d = GUI.get_screen_area("VIEW_3D")
	if context.scene.bsi_translate == True:
		context.scene.bsi_scale = False
		context.scene.bsi_rotate = False
		if view3d.show_manipulator == False:
			view3d.show_manipulator = True
			view3d.transform_manipulators = {'TRANSLATE'}

	else:
		view3d.show_manipulator = False

# drawing properties for viewport
bpy.types.Scene.boundbox			= BoolProperty( update = bsi_viewport_display("boundbox") )
bpy.types.Scene.wireframe			= BoolProperty( update = bsi_viewport_display("wireframe") )
bpy.types.Scene.constant			= BoolProperty( update = bsi_viewport_display("constant") )
bpy.types.Scene.solid				= BoolProperty( default = True, update = bsi_viewport_display("solid") )
bpy.types.Scene.textured			= BoolProperty( update = bsi_viewport_display("textured") )
bpy.types.Scene.textured_decal		= BoolProperty( update = bsi_viewport_display("textured_decal") )

# drawing properties for objects
bpy.types.Scene.occlude_wire		= BoolProperty( update = bsi_object_display("occlude_wire") )
bpy.types.Scene.show_wire			= BoolProperty( update = bsi_object_display("show_wire") )
bpy.types.Scene.show_x_ray			= BoolProperty( update = bsi_object_display("show_x_ray") )
bpy.types.Scene.show_all_edges		= BoolProperty( update = bsi_object_display("show_all_edges") )
bpy.types.Scene.show_axis			= BoolProperty( update = bsi_object_display("show_axis") )
bpy.types.Scene.show_bounds			= BoolProperty( update = bsi_object_display("show_bounds") )
bpy.types.Scene.show_name			= BoolProperty( update = bsi_object_display("show_name") )
bpy.types.Scene.show_only_shape_key	= BoolProperty( update = bsi_object_display("show_only_shape_key") )
bpy.types.Scene.show_transparent	= BoolProperty( update = bsi_object_display("show_transparent") )
bpy.types.Scene.show_texture_space	= BoolProperty( update = bsi_object_display("show_texture_space") )
bpy.types.Scene.backface_culling	= BoolProperty( update = bsi_object_display("backface_culling") )

#scale toggles
bpy.types.Scene.bsi_scale 			= bpy.props.BoolProperty(update=bsi_scale_toggle)
bpy.types.Scene.bsi_rotate 			= bpy.props.BoolProperty(update=bsi_rotate_toggle)
bpy.types.Scene.bsi_translate 		= bpy.props.BoolProperty(update=bsi_translate_toggle)

# default vectors for bsi home panel
bpy.types.Scene.scale = FloatVectorProperty(  
								name="scale",  
								default=(1.0, 1.0, 1.0),  
								subtype='XYZ',  
								description="scale"  
								)

bpy.types.Scene.rotation_euler = FloatVectorProperty(  
								name="rotation_euler",  
								default=(0.0, 0.0, 0.0),  
								subtype='XYZ',  
								description="rotation euler"  
								)

bpy.types.Scene.location = FloatVectorProperty(  
								name="location",  
								default=(0.0, 0.0, 0.0),  
								subtype='XYZ',  
								description="location"  
								)  


class BSI_Properties(PropertyGroup):
	shader_stand 		= BoolProperty(default=False)
	indirect_illum 		= BoolProperty(default=False)
	shader_rendering	= BoolProperty(default=False)
	shader_shadow		= BoolProperty(default=False)
	shader_sss 			= BoolProperty(default=False)
	shader_transparency	= BoolProperty(default=False)
	shader_reflection	= BoolProperty(default=False)
	shader_strand		= BoolProperty(default=False)

	diff_shader 		= BoolProperty(default=False)
	diff_image 			= BoolProperty(default=False)
	diff_path			= StringProperty()
	diff_uvs			= BoolProperty(default=False)
	diff_sampling		= BoolProperty(default=False)
	diff_influence 		= BoolProperty(default=False)
	diff_ramp 			= BoolProperty(default=False)
	diff_img_display	= BoolProperty(default=False)

	spec_shader 		= BoolProperty(default=False)
	spec_image 			= BoolProperty(default=False)
	spec_path			= StringProperty()
	spec_uvs			= BoolProperty(default=False)
	spec_sampling		= BoolProperty(default=False)
	spec_influence 		= BoolProperty(default=False)
	spec_ramp 			= BoolProperty(default=False)
	spec_img_display	= BoolProperty(default=False)
	
	tran_shader 		= BoolProperty(default=False)
	tran_image 			= BoolProperty(default=False)
	tran_path			= StringProperty()
	tran_uvs			= BoolProperty(default=False)
	tran_influence 		= BoolProperty(default=False)
	
	norm_shader 		= BoolProperty(default=False)
	norm_image 			= BoolProperty(default=False)
	norm_path			= StringProperty()
	norm_uvs			= BoolProperty(default=False)
	norm_influence 		= BoolProperty(default=False)
	
	refl_shader 		= BoolProperty(default=False)
	refl_image 			= BoolProperty(default=False)
	refl_path			= StringProperty()
	refl_uvs			= BoolProperty(default=False)
	refl_influence 		= BoolProperty(default=False)