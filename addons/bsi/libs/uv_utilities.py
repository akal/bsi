import bpy
import bmesh
import bmesh, bpy, mathutils, math
from collections import defaultdict
from math import cos, sin

class UvUtilities(object):
    def __init__(self):
        self.face_to_verts = defaultdict(set)
        self.vert_to_faces = defaultdict(set)
        self.selected_island = set()

    def set_uv_data(self, bmesh):
        self.bmesh = bmesh
        self.uvlayer = self.bmesh.loops.layers.uv.active
        for face in self.bmesh.faces:
            for loop in face.loops:
                id = '{0[0]:.5} {0[1]:.5} {1}'.format(loop[self.uvlayer].uv, loop.vert.index)
                self.face_to_verts[face.index].add(id)
                self.vert_to_faces[id].add(face.index)
                if loop[self.uvlayer].select :
                    self.selected_island.add(face.index)

    def get_island_face_ids(self):
        islands = []
        self.faces_left = set(self.face_to_verts.keys())
        while len(self.faces_left) > 0:
            face_id = list(self.faces_left)[0]
            self.current_island = []
            #print(self.faces_left)
            self.add_to_island(face_id)
            islands.append(self.current_island)
        return islands

    def add_to_island(self, face_id):
        if face_id in self.faces_left:
            #add the face itself
            self.current_island.append(face_id)
            #print(face_id)
            self.faces_left.remove(face_id)
            #and add all faces that share uvs with this face
            verts = self.face_to_verts[face_id]
            for vert in verts:
                #print('looking at vert {}'.format(vert))
                connected_faces = self.vert_to_faces[vert]
                if connected_faces:
                    for face in connected_faces:
                        self.add_to_island(face)     

    def uv_bbox(self, island):
        minX = +1000
        minY = +1000
        maxX = -1000
        maxY = -1000
        #for island in islands:
        #print(island)
        if hasattr(self.bmesh.faces, "ensure_lookup_table"): 
            self.bmesh.faces.ensure_lookup_table()
        for face_id in island:
            face = self.bmesh.faces[face_id]
            for loop in face.loops:
                minX = min(loop[self.uvlayer].uv.x, minX)
                minY = min(loop[self.uvlayer].uv.y, minY)
                maxX = max(loop[self.uvlayer].uv.x, maxX)
                maxY = max(loop[self.uvlayer].uv.y, maxY)

        return mathutils.Vector((minX,minY)), mathutils.Vector((maxX,maxY))

    def uv_bbox_center(self, island):
        minX = +1000
        minY = +1000
        maxX = -1000
        maxY = -1000
        #for island in islands:
        for face_id in island:
            face = self.bmesh.faces[face_id]
            for loop in face.loops:
                minX = min(loop[self.uvlayer].uv.x, minX)
                minY = min(loop[self.uvlayer].uv.y, minY)
                maxX = max(loop[self.uvlayer].uv.x, maxX)
                maxY = max(loop[self.uvlayer].uv.y, maxY)
                
        return  (mathutils.Vector((minX,minY))+ mathutils.Vector((maxX,maxY)))/2

    def island_direction(self, island):
        bbox = self.uv_bbox(island)
        
        width = abs(bbox[0].x - bbox[1].x)
        height = abs(bbox[0].y - bbox[1].y)
        
        print(width, height)
        if width > height:
            return "Horizontal"
        elif width == height:
            return "Same"
        else:
            return "Vertical"
        
    def move_islands(self, vector, islands):        
        #for island in islands:
            for face_id in island:
                face = self.bmesh.faces[face_id]
                #print(face_id)
                for loop in face.loops:
                    #print(loop[uvlayer].uv) 
                    loop[self.bmesh.loops.layers.uv.active].uv += vector
                    #print(loop[self.bmesh.loops.layers.uv.active].uv) 

    def add_uv_channel(context):
        obj = context.active_object
        if context.mode != 'EDIT_MESH':
                bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.uv_texture_add()
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.uv.cube_project(scale_to_bounds=True)

    def xform_rotation(self, angle, origin=(0, 0)):
        cos_theta, sin_theta = cos(angle), sin(angle)
        x0, y0 = origin
        def xform(point):
            x, y = point[0] - x0, point[1] - y0
            return (x * cos_theta - y * sin_theta + x0,
                    x * sin_theta + y * cos_theta + y0)
        return xform