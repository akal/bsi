import bpy
import os
import bpy.utils.previews
from bpy_extras import view3d_utils
from bpy.props import StringProperty


class GuiUtilities(object):
	def __init__(self):
		r""" """
	def button_slider(self, layout, texture_slot, toggle, factor, name):
		r""" """
		row = layout.row(align=True)
		row.prop(texture_slot, toggle, text="")
		sub = row.row(align=True)
		sub.active = getattr(texture_slot, toggle)
		sub.prop(texture_slot, factor, text=name, slider=True)
		return sub  # XXX, temp. use_map_normal needs to override.
		
	def toggle_object_visibilities(self, vis_type):
		code_l =[
					"vis = []",
					"hid = []",
					"for ob in bpy.data.objects:",
					"	if ob.%s:",
					"		vis.append(ob)",
					"	else:",
					"		hid.append(ob)",
					"if len(vis) < len(hid):",
					"	for ob in bpy.data.objects:",
					"		ob.%s = True",
					"else:",
					"	for ob in bpy.data.objects:",
					"		ob.%s = False",
				]
		code = "\n".join(code_l)
		exec (code % (vis_type,vis_type,vis_type))
		
	def context_tex_datablock(self, context):
		idblock = context.material
		if idblock:
			return active_node_mat(idblock)

		idblock = context.lamp
		if idblock:
			return idblock

		idblock = context.world
		if idblock:
			return idblock

		idblock = context.brush
		if idblock:
			return idblock

		idblock = context.line_style
		if idblock:
			return idblock

		if context.particle_system:
			idblock = context.particle_system.settings

		return idblock

	def header_decorator(self, string, repeat=50):
		decorator = "-"*repeat
		return "  :: %s %s" % (string, decorator)

	def image_filter_glob(self):
		return StringProperty(
								default="*.bmp;*.sgi;*.rgb;*.bw;*.png;*.jpg;*.jpeg;*.exr;*.hdr;*.jp2;*.j2c;*.tga", 
								options={'HIDDEN'}
							 )
	
	def image_file_path(self):
		return bpy.props.StringProperty(
											name="File Path", 
											description="File path use", 
											maxlen= 1024, default= ""
										)

	def get_blender_3d_node_types(self):
		return ["EMPTY", "LAMP", "CAMERA", "SPEAKER", "LATTICE", "ARMATURE", "FONT", "META", "SURFACE", "CURVE", "MESH", "FORCE"]

	def get_screen_area(self, type = "VIEW_3D"):
		for area in bpy.context.screen.areas:
			if area.type == type:
				return area.spaces[0]

	def get_screen_region(self, _type = 'UI'):
		for region in bpy.context.area.regions:
			if region.type == _type:
				return region
		return False

	def get_bbox_center(self, obj):
		x=[]
		y=[]
		z=[]

		bbox = [itm[:] for itm in obj.bound_box]
		for each in (bbox):
			x.append(each[0])
			y.append(each[1])
			z.append(each[2])

		sorted(x)
		sorted(y)
		sorted(z)

		return [((x[0]+x[-1])/2),((y[0]+y[-1])/2),((z[0]+z[-1])/2)]
		
	def get_view3d_cursor(self):
		'''finds the toggle of the cursor by "author":"dairin0d",'''
		try:
			screen = bpy.data.screens.get("Default", bpy.data.screens[0])
		except:
			screen = bpy.context.window_manager.windows[0].screen

		try:
			settings = screen.cursor_3d_tools_settings
		except:
			# addon was unregistered
			settings = None

		return settings

	def mouse_cursor_reset(self, context, pos_x, pos_y):
		context.window.cursor_warp(pos_x, pos_y)
	
	def cursor_hide(self, context):
		context.window.cursor_modal_set('NONE')
	
	def cursor_restore(self, context):
		context.window.cursor_modal_restore()
	
	def visible_objects_and_duplis(self, context):
		"""Loop over (object, matrix) pairs (mesh only)"""

		for obj in context.visible_objects:
			if obj.type == 'MESH':
				yield (obj, obj.matrix_world.copy())

			if obj.dupli_type != 'NONE':
				obj.dupli_list_create(scene)
				for dob in obj.dupli_list:
					obj_dupli = dob.object
					if obj_dupli.type == 'MESH':
						yield (obj_dupli, dob.matrix.copy())

			obj.dupli_list_clear()

	def obj_ray_cast(self, obj, matrix, ray_origin, ray_target):
		"""Wrapper for ray casting that moves the ray into object space"""

		# get the ray relative to the object
		matrix_inv = matrix.inverted()
		ray_origin_obj = matrix_inv * ray_origin
		ray_target_obj = matrix_inv * ray_target

		# cast the ray
		hit, normal, face_index = obj.ray_cast(ray_origin_obj, ray_target_obj)

		if face_index != -1:
			print(hit, normal, face_index)
			return hit, normal, face_index
		else:
			return None, None, None
			
	def pick_object(self, context, event, ray_max=1000.0):
		"""Run this function on left mouse, execute the ray cast"""
		# get the context arguments
		scene = context.scene
		region = context.region
		rv3d = context.region_data
		coord = event.mouse_region_x, event.mouse_region_y

		# get the ray from the viewport and mouse
		view_vector = view3d_utils.region_2d_to_vector_3d(region, rv3d, coord)
		ray_origin = view3d_utils.region_2d_to_origin_3d(region, rv3d, coord)

		ray_target = ray_origin + (view_vector * ray_max)

		# cast rays and find the closest object
		best_length_squared = ray_max * ray_max
		best_obj = None

		for obj, matrix in self.visible_objects_and_duplis(context):
			if obj.type == 'MESH':
				hit, normal, face_index = self.obj_ray_cast(obj, matrix, ray_origin, ray_target)
				if hit is not None:
					hit_world = matrix * hit
					# scene.cursor_location = hit_world
					length_squared = (hit_world - ray_origin).length_squared
					if length_squared < best_length_squared:
						best_length_squared = length_squared
						best_obj = obj
						

		# now we have the object under the mouse cursor,
		# we could do lots of stuff but for the example just select.
		if best_obj is not None:
			return best_obj	
			
	# def get_ui_theme(self, context):
	# 	ct = context.user_preferences.themes.items()[0][0] 
	# 	return context.user_preferences.themes[ct].user_interface
	def unregister_property_panels(self):
		panels = [
					"GreasePencilDrawingToolsPanel",
					"VIEW3D_PT_tools_grease_pencil_draw",
					"VIEW3D_PT_tools_grease_pencil_edit",
					"GreasePencilStrokeEditPanel",
					"GreasePencilToolsPanel",
					"VIEW3D_PT_tools_animation",
					"VIEW3D_PT_tools_rigid_body",
					"VIEW3D_PT_tools_relations",
					"VIEW3D_PT_tools_add_object",
					"VIEW3D_PT_grease_pencil",
					"VIEW3D_PT_view3d_display",
					"VIEW3D_PT_view3d_name",
					"VIEW3D_PT_view3d_cursor",
					"VIEW3D_PT_view3d_properties",
					# "VIEW3D_PT_view3d_stereo",
					"VIEW3D_PT_view3d_shading",
					"VIEW3D_PT_view3d_motion_tracking",
					"VIEW3D_PT_tools_meshedit",

					"VIEW3D_PT_tools_shading", 
					# "VIEW3D_PT_tools_uvs",
					"VIEW3D_PT_tools_transform",
					"VIEW3D_PT_tools_meshedit_options",
					"VIEW3D_PT_tools_meshweight",
					"VIEW3D_PT_view3d_meshdisplay",
					"VIEW3D_PT_view3d_meshstatvis",
					"VIEW3D_PT_tools_object",
					"VIEW3D_PT_tools_history",

					# "VIEW3D_PT_tools_add_object",
					# "VIEW3D_PT_tools_relations",
					# "VIEW3D_PT_tools_animation",
					# "VIEW3D_PT_tools_rigid_body",
					"VIEW3D_PT_tools_transform_mesh",
					# "VIEW3D_PT_tools_meshedit",
					# "VIEW3D_PT_tools_meshweight",
					"VIEW3D_PT_tools_add_mesh_edit",
					# "VIEW3D_PT_tools_shading",
					# "VIEW3D_PT_tools_uvs",
					# "VIEW3D_PT_tools_meshedit_options",
					# "VIEW3D_PT_tools_transform_curve",
					# "VIEW3D_PT_tools_curveedit",
					# "VIEW3D_PT_tools_add_curve_edit",
					# "VIEW3D_PT_tools_transform_surface",
					# "VIEW3D_PT_tools_surfaceedit",
					# "VIEW3D_PT_tools_add_surface_edit",
					# "VIEW3D_PT_tools_textedit",
					# "VIEW3D_PT_tools_armatureedit_transform",
					# "VIEW3D_PT_tools_armatureedit",
					# "VIEW3D_PT_tools_armatureedit_options",
					# "VIEW3D_PT_tools_mballedit",
					# "VIEW3D_PT_tools_add_mball_edit",
					# "VIEW3D_PT_tools_latticeedit",
					# "VIEW3D_PT_tools_posemode",
					# "VIEW3D_PT_tools_posemode_options",
					# "View3DPaintPanel",
					# "VIEW3D_PT_imapaint_tools_missing",
					# "VIEW3D_PT_tools_brush",
					# "TEXTURE_UL_texpaintslots",
					# "VIEW3D_MT_tools_projectpaint_uvlayer",
					# "VIEW3D_PT_slots_projectpaint",
					# "VIEW3D_PT_stencil_projectpaint",
					# "VIEW3D_PT_tools_brush_overlay",
					# "VIEW3D_PT_tools_brush_texture",
					# "VIEW3D_PT_tools_mask_texture",
					# "VIEW3D_PT_tools_brush_stroke",
					# "VIEW3D_PT_tools_brush_curve",
					# "VIEW3D_PT_sculpt_dyntopo",
					# "VIEW3D_PT_sculpt_options",
					# "VIEW3D_PT_sculpt_symmetry",
					# "VIEW3D_PT_tools_brush_appearance",
					# "VIEW3D_PT_tools_weightpaint",
					# "VIEW3D_PT_tools_weightpaint_options",
					# "VIEW3D_PT_tools_vertexpaint",
					# "VIEW3D_PT_tools_imagepaint_external",
					# "VIEW3D_PT_tools_imagepaint_symmetry",
					# "VIEW3D_PT_tools_projectpaint",
					# "VIEW3D_PT_imagepaint_options",
					# "VIEW3D_MT_tools_projectpaint_stencil",
					# "VIEW3D_PT_tools_particlemode",
					# "VIEW3D_PT_tools_grease_pencil_draw",
					# "VIEW3D_PT_tools_grease_pencil_edit",
					# "VIEW3D_PT_tools_history",
					# "VIEW3D_PT_view3d_meshdisplay",
					# "VIEW3D_PT_view3d_meshstatvis",
					# "VIEW3D_PT_view3d_meshdisplay",
					# "VIEW3D_PT_view3d_meshstatvis",
					# "VIEW3D_PT_view3d_curvedisplay",
					# "VIEW3D_PT_background_image",
					# "VIEW3D_PT_transform_orientations",
					# "VIEW3D_PT_etch_a_ton",
					# "VIEW3D_PT_context_properties"
				]

		for panel in panels:
			try:
				exec ("pn = bpy.types.%s" % panel)
				exec ("bpy.utils.unregister_class(pn)")
			except:
				pass

		

def get_icons(ico_path):
	ico_coll = bpy.utils.previews.new()

	for each in os.listdir(ico_path):
		tkns = each.lower().split(".")
		if tkns[-1] == "png":
			ico_coll.load(tkns[0], "%s/%s" % (ico_path,each), "IMAGE")
	return ico_coll

def del_icons():
	collection = bpy.utils.previews.ImagePreviewCollection()
	for pcoll in collection.values():
		bpy.utils.previews.remove(pcoll)
	collection.clear()

def delete_object(ob):
	if ob == None:
		data = None
		if ob.type in ['LAMP', 'CAMERA','CURVE']:
			cmd = "bpy.data.%ss['%s']" % (ob.type.lower(), ob.name)
			data = eval(cmd)
		
		if ob.type == 'EMPTY':
			data = bpy.data.objects[ob.name]
			data.user_clear()
			bpy.data.objects.remove(data)
			bpy.ops.object.delete(use_global=False)
			return
		
		if ob.type == 'MESH':
			data = bpy.data.meshes[ob.name]
			data.user_clear()
			bpy.data.meshes.remove(data)
			bpy.ops.object.delete(use_global=False)
			return

		data.user_clear()
		cmd = "bpy.data.%ss.remove(data)"  % ob.type.lower()
		eval (cmd)
		bpy.ops.object.delete(use_global=False)
		return True
		

