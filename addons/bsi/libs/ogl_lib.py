import bgl
from bpy import context

def cursor_reset(self, context, pos_x, pos_y):
		context.window.cursor_warp(pos_x, pos_y)
		
def gl_cursor2():
	'''Pointer'''
	genList = bgl.glGenLists(1)
	bgl.glNewList(genList,bgl.GL_COMPILE)
	i = 8
	bgl.glBegin(bgl.GL_TRIANGLES)
	bgl.glColor3f(0.2, 0.2, 0.2)
	bgl.glVertex2f(i, i - 7 )
	bgl.glVertex2f(-i, i)
	bgl.glVertex2f(-i+7,-i)
	bgl.glEnd()
	bgl.glBegin(bgl.GL_TRIANGLES)

	bgl.glColor3f(1, 1, 1)
	bgl.glVertex2f(i-2, i - 9 )
	bgl.glVertex2f(-i+3, i-3)
	bgl.glVertex2f(-i+9,-i+2)
	bgl.glEnd()
	bgl.glEndList()
	return genList

def gl_cursor():
	'''Pointer'''
	genList = bgl.glGenLists(1)
	bgl.glNewList(genList,bgl.GL_COMPILE)
	bgl.glBegin(bgl.GL_TRIANGLES)
	bgl.glColor3f(0.15, 0.15, 0.15)
	bgl.glVertex2f( -3.0 , 6.0 )
	bgl.glVertex2f( 6.0 , 9.0 )
	bgl.glVertex2f( -15.0 , 18.0 )
	bgl.glVertex2f( -6.0 , -3.0 )
	bgl.glVertex2f( -3.0 , 6.0 )
	bgl.glVertex2f( -15.0 , 18.0 )


	bgl.glEnd()
	bgl.glBegin(bgl.GL_TRIANGLES)
	bgl.glColor3f(.8, .8, .8)
	bgl.glVertex2f( -6.0 , 2.627403438091278 )
	bgl.glVertex2f( -4.895723462104797 , 7.808330297470093 )
	bgl.glVertex2f( -11.882307529449463 , 14.824045658111572 )
	bgl.glVertex2f( -0.0643683671951294 , 9.0 )
	bgl.glVertex2f( -11.882307529449463 , 14.824045658111572 )
	bgl.glVertex2f( -4.895723462104797 , 7.808330297470093 )




	bgl.glEnd()

	bgl.glEndList()
	return genList






# import bpy  
  
# current_obj = bpy.context.active_object  
  
# print("="*40) # printing marker  
# for face in current_obj.data.polygons:  
#     verts_in_face = face.vertices[:]  
#     #print("face index", face.index)  
#     #pprint("normal", face.normal)  
#     for vert in verts_in_face:  
#         print("bgl.glVertex2f(", current_obj.data.vertices[vert].co.x*3, ",", 
#                                             current_obj.data.vertices[vert].co.y*3, ")")  
