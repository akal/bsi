import bpy, os

class MaterialUtilities(object):
	
	def __init__(self):
		r""""""
		self.ports_dct = {"diffuse":0, "specular":1,}

	def get_file_name(self, file_path):
		r"""returns a name of the file without the extension"""
		return os.path.basename(file_path).split(".")[0]

	def get_image_node(self, file_path):
		r""" """
		image_name = self.get_file_name(file_path)
		for b_img in bpy.data.images:
			if image_name == b_img.name:
				return b_img

		b_img = bpy.data.images.load(file_path) 
		b_img.name = image_name
		return b_img

	def get_texture_node(self,file_path):
		r""" """
		image_name = self.get_file_name(file_path)
		for tex in bpy.data.textures: 
			if tex.name == image_name: 
				return tex 
		return bpy.data.textures.new(image_name, type='IMAGE') 

	def clear_texture_slot(self, material, port_type="diffuse"):
		r"""clear a slot by type """
		material.texture_slots.clear(self.ports_dct[port_type])
		return True

	def get_texture_slot(self, material, port_type):
		r"""gets a slot by type of hook up"""
		slot_id = self.ports_dct[port_type]
		slot = material.texture_slots.create(slot_id)
		if slot == None:
			slot = material.texture_slots.create(slot_id)
		return slot

	def set_uv_texture_attr(self, texture_slot):
		r""" """
		texture_slot.texture_coords = 'UV' 
		texture_slot.mapping = 'FLAT' 

	def set_diffuse_texture_attr(self, texture_slot, texture_node):
		r""" """
		texture_slot.texture = texture_node
		texture_slot.use_map_color_diffuse = True  
		texture_slot.use_map_color_emission = True  
		texture_slot.emission_color_factor = 0.5
		texture_slot.use_map_density = True

	def check_material(self, material):
		if material is not None:
			if material.use_nodes:
				if material.active_node_material is not None:
					return True
				return False
			return True
		return False
