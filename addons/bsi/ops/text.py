import bpy  
from bpy.types import Operator  

class BSI_TEXT_reload_run(Operator):
    """BSI reload and run text"""
    bl_idname = "bsi.reload_run_text"
    bl_label = "Reload Run Text "
    bl_options = {'REGISTER'}

    def execute(self, context):
        bpy.ops.text.reload()
        bpy.ops.text.run_script()
        return {'FINISHED'}