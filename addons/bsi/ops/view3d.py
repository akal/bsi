import bpy, blf, bgl
import time
from threading import Thread
from bpy.types import Operator
from mathutils import Matrix
from bpy.props import StringProperty, IntProperty, FloatVectorProperty
from bpy_extras import view3d_utils
from ..libs import gui_utilities

############################### CONSTANTS ###############################
GUI = gui_utilities.GuiUtilities()
############################### CONSTANTS ###############################

class BSI_VIEW3D_unlock_global_transforms(Operator):
    """bsi global unlock transforms"""
    bl_idname = "bsi.view3d_unlock_transforms"
    bl_label = "set transforms"
    bl_options = {'REGISTER'}

    type = StringProperty()

    def execute(self, context):
        for ob in bpy.data.objects:
            if self.type == "SCALE":
                ob.lock_scale = (False, False, False)
            if self.type == "ROTATE":
                ob.lock_rotation = (False, False, False)
            if self.type == "TRANSLATE":
                ob.lock_location = (False, False, False)

        return {'FINISHED'}

class BSI_VIEW3D_toggle_manipulator(Operator):
    """Show Manipulator Op"""
    bl_idname = "bsi.view3d_toggle_manipulator"
    bl_label = "Toggle Manipulator"
    bl_options = {'REGISTER'}
    m_type = StringProperty(default="TRANSLATE")
    c_state = StringProperty()

    def execute(self, context):

        view3d = GUI.get_screen_area("VIEW_3D")
        # attrs = ['show_manipulator']
        # for attr, toggle in map(lambda attr:(attr, getattr(view3d, attr)), attrs):
        #   setattr(view3d, attr, not toggle)
        if view3d.show_manipulator == False:
            view3d.show_manipulator = True

        view3d.transform_manipulators = {self.m_type}
        
        return {'FINISHED'} 

class BSI_VIEW3D_node_visibilty_toggle(Operator):
    """Tooltip"""
    bl_idname = "bsi.view3d_node_visibilty_toggle"
    bl_label = "Node Visibilty Toggle"

    node = bpy.props.StringProperty(default= "MESH")

    def execute(self, context):
        node_types = GUI.get_blender_3d_node_types() 
        hidden = []
        visible = []
        
        if self.node == "SHOW_ALL":
            for ob in context.scene.objects: ob.hide=False
        
        if self.node == "HIDE_ALL":
            for ob in context.scene.objects: ob.hide=True
        
                        
        if self.node in node_types:

            #case for FORCE OBJECTS
            if self.node == "FORCE":
                for ob in context.scene.objects:
                    if ob.type == "EMPTY" and ob.field.type != None:
                        if ob.hide:
                            hidden.append(ob)
                        else:
                            visible.append(ob)
            else:
                for ob in context.scene.objects:
                    if ob.type == self.node:
                        if ob.hide:
                            hidden.append(ob)
                        else:
                            visible.append(ob)
            #do the toggling
            if len(hidden) > len(visible):
                for node in hidden:
                    node.hide = False
            
            else:
                for node in visible:
                    node.hide = True

        return {'FINISHED'}

class BSI_VIEW3D_set_background_color(Operator):
    """BSI Set background color"""
    bl_idname = "bsi.view3d_set_background_color"
    bl_label = "Change Background"
    bl_options = {'REGISTER'}
    color = FloatVectorProperty(
                                name="direction",  
                                default=(0.0, 0.0, 0.0),  
                                subtype='XYZ',  
                                description="move direction"  
                                )

    def execute(self, context):
        bpy.context.user_preferences.themes[0].view_3d.space.gradients.high_gradient = self.color
        bpy.ops.wm.save_userpref()
        return {'FINISHED'}

class View3DTextDisplay():
    """Text Display in View3d"""
    def __init__(self):
        self.crl = (1,0,0)
        self.message = ""
        # self.message
        self._handle = bpy.types.SpaceView3D.draw_handler_add(self.__draw_handler, tuple(), 'WINDOW', 'POST_PIXEL')

    def __draw_handler(self):
        bgl.glColor4f(self.crl[0],self.crl[1], self.crl[2], 1)
        blf.position(0,70,30,0)
        blf.size(0,20,72)
        blf.draw(0,self.message)

    def remove(self):
        if not self._handle: 
            return
        bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
    
    def show(self):
        # self._msg = str(msg)
        # self._color = _color
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

    def clear(self):
        self._msg = str()
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

class BSI_VIEW3D_text_display():
    """Text Display in View3d"""
    def __init__(self):
        self.crl = (1,0,0)
        self.message = ""
        # self.message
        self._handle = bpy.types.SpaceView3D.draw_handler_add(self.__draw_handler, tuple(), 'WINDOW', 'POST_PIXEL')
        
    def __draw_handler(self):
        # bgl.glColor4f(self._color)
        bgl.glColor4f(self.crl[0],self.crl[1], self.crl[2], 1)
        # bgl.glColor4f(1,0,1,1)
        blf.position(0,70,30,0)
        blf.size(0,20,72)
        blf.draw(0,self.message)

    def remove(self):
        if not self._handle: 
            return
        bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
    
    def show(self):
        # self._msg = str(msg)
        # self._color = _color
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

    def clear(self):
        self._msg = str()
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

class BSI_VIEW3D_redraw_windows(Operator):
    """Redraws all windows"""
    bl_idname = "bsi.wm_redraw_all"
    bl_label = "BSI Redraw"
    bl_options = {'REGISTER', 'UNDO'}
    
    def execute(self, context):
        context = bpy.context
        for window in context.window_manager.windows:
            for area in window.screen.areas:
                if area.type == 'VIEW_3D':
                    for region in area.regions:
                        if region.type == 'WINDOW':
                            region.tag_redraw()
        return {'FINISHED'}

class BSI_VIEW3D_cursor_to_bounds(Operator):
    """BSI Cursor to bounds"""
    bl_idname = "bsi.view3d_cursor_to_bounds"
    bl_label = "BSI Cursor to Bounds"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        obj = context.active_object
        bpy.ops.bsi.object_pivot_to_selected()
        context.space_data.pivot_point ='BOUNDING_BOX_CENTER'

        return {'FINISHED'}

class BSI_VIEW3D_shading_display(Operator):
    """BSI Shading Display"""
    bl_idname = "bsi.view3d_shading"
    bl_label = "Shading Display"
    bl_options = {'REGISTER', 'UNDO'}
    # 'BOUNDBOX', 'WIREFRAME' 'SOLID' 'TEXTURED' 'MATERIAL' 'RENDERED'

    types = bpy.props.StringProperty() # defining the property

    def execute(self, context):
        view3d = GUI.get_screen_area("VIEW_3D")
        view3d.viewport_shade = self.types
        return {'FINISHED'}

        bpy.context.space_data.show_textured_solid = True

class BSI_VIEW3D_toggle_grid(Operator):
    """SI Toggle Grid"""
    bl_idname = "bsi.view3d_toggle_grid"
    bl_label = "BSI Toggle Grid"
    bl_options = {'REGISTER'}
    
    def execute( self, context ):
        view3d = GUI.get_screen_area("VIEW_3D")
        attrs = ['show_floor']
        for attr, toggle in map(lambda attr:(attr, getattr(view3d, attr)), attrs):
            setattr(view3d, attr, not toggle)
        return {'FINISHED'}

class BSI_VIEW3D_toggle_axis(Operator):
    """SI Toggle Axis"""
    bl_idname = "bsi.view3d_toggle_axis"
    bl_label = "BSI Toggle Grid"
    bl_options = {'REGISTER'}
    
    def execute( self, context ):
        view3d = GUI.get_screen_area("VIEW_3D")
        attrs = ['show_axis_x','show_axis_y', 'show_axis_z' ]
        for attr, toggle in map(lambda attr:(attr, getattr(view3d, attr)), attrs):
            setattr(view3d, attr, not toggle)
        return {'FINISHED'}

class BSI_VIEW3D_reset_view(Operator):
    """SI Reset View"""
    bl_idname = "bsi.view3d_reset_view"
    bl_label = "BSI Reset View"
    bl_options = {'REGISTER'}
    
    def execute( self, context ):
        view3d = GUI.get_screen_area("VIEW_3D")
        view3d.region_3d.view_matrix = Matrix((
                                             (0.41, 0.91, -0.00, 0.0),
                                             (-0.40, 0.19, 0.89, 0.0),
                                             (0.81, -0.36, 0.44, -14.98),
                                             (0.0, 0.0, 0.0, 1.0)
                                            ))
        return {'FINISHED'}

class BSI_VIEW3D_diplay_text(Operator):
    """SI Display Text"""
    bl_idname = "bsi.view3d_display_text"
    bl_label = "Display Text"
    bl_options = {'REGISTER'}
    message = StringProperty(  
                            name="message",  
                            default="",  
                            description="Text to display in viewport"  
                            ) 
    amount = IntProperty(  
                            name="amount",  
                            default=1,  
                            description="time of message is displayed"  
                            ) 
    # amount = 5
    def threaded_sleep(self, i):
        # print ("sleeping 5 sec from thread %d" % i)
        time.sleep(self.amount)
        # print ("run some stuff here %d" % i)
        BSI_V3DTD.message = ""
        BSI_V3DTD.clear()

    def execute( self, context ):
        # BSI_V3DTD.message = "Hello World"
        BSI_V3DTD.clr = (1,1,0)
        BSI_V3DTD.message = self.message
        BSI_V3DTD.clear()
        BSI_V3DTD.show()

        if self.amount > 0:
            for i in range(2):
                t = Thread(target=self.threaded_sleep, args=(i,))
                t.start()

        return {'FINISHED'}

    @classmethod  
    def poll(cls, context):  
        ob = context.active_object  
        return ob is not None and ob.mode == 'OBJECT'  

