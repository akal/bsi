import bpy
from bpy.types import Operator
from ..libs import mesh_utilities
from ..libs import object_utilities

############################### CONSTANTS ###############################
MSH = mesh_utilities.MeshUtilities()
OBJ = object_utilities.ObjectUtilities()
############################### CONSTANTS ###############################

class BSI_MESH_subdivision_sub(Operator):
	"""De-increments Subdivision Levels by -1"""
	bl_idname = "bsi.mesh_subd_dcr"
	bl_label = "BSI SubD \"-\""
	bl_options = {'REGISTER',}

	@classmethod
	def poll(cls, context):
		obs = context.selected_editable_objects
		return (obs is not None)

	def execute(self, context):
		mod_name = 'SUBSURF'
		for obj in context.selected_editable_objects:

			mod = MSH.get_modifier(obj, mod_name)
			if mod:
				val = int(mod.levels)
				val -= 1
				if val > 0:
					mod.levels = val
					mod.render_levels = val
				if val <= 0:
					obj.modifiers.remove(mod)

		bpy.ops.bsi.wm_redraw_all()
		return {'FINISHED'}

class BSI_MESH_subdivision_add(Operator):
	"""Increments Subdivision Levels by +1"""

	bl_idname = "bsi.mesh_subd_incr"
	bl_label = "BSI SubD \"+\""
	bl_options = {'REGISTER'}

	@classmethod
	def poll(cls, context):
		obs = context.selected_editable_objects
		return (obs is not None)

	def execute(self, context):
		mod_name = 'SUBSURF'
		for obj in context.selected_editable_objects:
			mod = MSH.get_modifier(obj, mod_name)
			if not mod:
				mod = MSH.set_modifier(obj, mod_name)
				mod.render_levels = 1
				bpy.ops.bsi.wm_redraw_all()
				return {'FINISHED'}
			if mod:
				val = int(mod.levels)
				if val < 10:
					mod.levels += 1
					mod.render_levels += 1
		bpy.ops.bsi.wm_redraw_all()
		return {'FINISHED'}

class BSI_MESH_collapse(Operator):
	"""SI Merge Center"""
	bl_idname = "bsi.view3d_collapse"
	bl_label = "BSI Merge Center"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		if context.selected_editable_objects:
			bpy.ops.mesh.merge(type='CENTER')
			return {'FINISHED'}
		else:
			self.report({'INFO'}, "Blender node not editable")
			return {'CANCELLED'}
			
class BSI_MESH_edge_mode(Operator):
	"""SI Edge Mode"""
	bl_idname = "bsi.view3d_edge_mode"
	bl_label = "BSI Edge/Segment Mode"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):

		if OBJ.is_editable(context.active_object):
			bpy.ops.object.mode_set(mode="EDIT")
			bpy.context.tool_settings.mesh_select_mode = (False, True , False)
			bpy.context.tool_settings.uv_select_mode = 'EDGE' # instead 'EDGE','FACE','ISLAND'
			return {'FINISHED'}
		else:
			self.report({'INFO'}, "Blender node not editable")
			return {'CANCELLED'}

class BSI_MESH_face_mode(Operator):
	"""SI Face Mode"""
	bl_idname = "bsi.view3d_face_mode"
	bl_label = "BSI Face Mode"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):

		if OBJ.is_editable(context.active_object):
			bpy.ops.object.mode_set(mode="EDIT")
			bpy.context.tool_settings.mesh_select_mode = (False , False , True)
			bpy.context.tool_settings.uv_select_mode = 'FACE'
			return {'FINISHED'}
		else:
			self.report({'INFO'}, "Blender node not editable")
			return {'CANCELLED'}

class BSI_MESH_object_mode(Operator):
	"""SI Object Mode"""
	bl_idname = "bsi.view3d_object_mode"
	bl_label = "BSI Object Mode"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		bpy.ops.object.mode_set(mode="OBJECT")
		return {'FINISHED'}
	
class BSI_MESH_vertex_mode(Operator):
	"""SI Vertex Mode"""
	bl_idname = "bsi.view3d_vertex_mode"
	bl_label = "BSI Vertex Mode"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		if OBJ.is_editable(context.active_object):
			bpy.ops.object.mode_set(mode="EDIT")
			bpy.context.tool_settings.mesh_select_mode = (True , False , False)
			bpy.context.tool_settings.uv_select_mode = 'VERTEX' # instead 'EDGE','FACE','ISLAND'
			return {'FINISHED'}
		else:
			self.report({'INFO'}, "Blender node not editable")
			return {'CANCELLED'}

class BSI_MESH_select_ring(Operator):
	"""SI RC Tools Ring Selection"""
	bl_idname = "bsi.view3d_ring_selection"
	bl_label = "BSI Ring Selection"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):

		if OBJ.is_editable(context.active_object):
			bpy.ops.mesh.loop_multi_select(ring=True)
			return {'FINISHED'}
		else:
			self.report({'INFO'},
						"No editable selected objects, could not finish")
			return {'CANCELLED'}

class BSI_MESH_detach(Operator):
	"""SI detach"""
	bl_idname = "bsi.mesh_detach"
	bl_label = "BSI Detach"
	bl_options = {'REGISTER', 'UNDO'}
	
	delete = bpy.props.BoolProperty()

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		obj = bpy.context.active_object
		mode = MSH.get_selection_mode()
		if mode == 'FACE':
			if not self.delete:
				bpy.ops.mesh.duplicate()
	
			obs_start = list(bpy.data.objects)
			bpy.ops.mesh.separate(type='SELECTED')
			obs_fin = list(bpy.data.objects)
			new_ob = list(set(obs_fin).difference(set(obs_start)))
			bpy.ops.object.mode_set(mode="OBJECT")
			obj.select = False
			new_ob[0].select = True
			bpy.context.scene.objects.active = new_ob[0]
			bpy.ops.bsi.view3d_face_mode()
			bpy.ops.mesh.select_all(action='SELECT')
			MSH.set_origin_to_selected(obj)

			return {'FINISHED'}
		else:
			self.report({'INFO'}, "Tool operates in face mode")
			return {'CANCELLED'}
		
		return {'FINISHED'}

class BSI_MESH_bevel(Operator):
	"""SI Bevels based on selection"""
	bl_idname = "bsi.mesh_bevel"
	bl_label = "BSI Bevel"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		ob = context.active_object
		bpy.ops.mesh.bevel(offset_type='OFFSET', offset=0, segments=1, profile=0.5, vertex_only=False, clamp_overlap=False, material=-1)
		print ("here")
		# bpy.ops.mesh.bevel()
		# if ob.mode == 'EDIT':
		# 	mode = context.tool_settings.mesh_select_mode
		# 	if mode[0] == True:
		# 		bpy.ops.mesh.bevel(vertex_only=True)
		# 	else: 
		# 		bpy.ops.mesh.bevel()
		return {'FINISHED'}

class BSI_MESH_delete(Operator):
	"""IB Delete Style"""
	bl_idname = "bsi.mesh_delete"
	bl_label = "BSI Delete"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		obj = context.active_object
		if obj == None:
			bpy.ops.object.delete(use_global=False)
		else:
			if obj.mode == 'EDIT':
				mode = MSH.get_selection_mode()

				if mode == 'FACE':
					bpy.ops.mesh.delete(type='FACE')

				if mode == 'EDGE':
					bpy.ops.mesh.dissolve_edges()

				if mode == 'VERT':
					bpy.ops.mesh.dissolve_verts()

			if obj.mode == 'OBJECT':
				bpy.ops.object.delete(use_global=False)
			
		return {'FINISHED'}

class BSI_OBJECT_pivot_to_selected( Operator ):
	"""Pivot to Selection"""
	bl_idname = "bsi.object_pivot_to_selected"
	bl_label = "BSI Pivot To Selected"
	bl_options = {'REGISTER', 'UNDO'}
 
	def execute(self, context):
		obj = context.active_object
		MSH.set_origin_to_selected(obj)

		return {'FINISHED'}

