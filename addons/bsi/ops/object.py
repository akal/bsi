import bpy, bgl, blf
from bpy.types import Operator  
from bpy.props import FloatVectorProperty, FloatProperty, BoolProperty 
import mathutils
from ..libs import ogl_lib as OGLW
from ..libs import gui_utilities
from ..libs import object_utilities

############################### CONSTANTS ###############################
OBJ = object_utilities.ObjectUtilities()
GUI = gui_utilities.GuiUtilities()
############################### CONSTANTS ###############################

class BSI_OBJECT_select_tree(Operator):
	"""Select tree op"""
	bl_idname = "bsi.object_select_tree"
	bl_label = "Select Tree"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		node = bpy.context.active_object
		OBJ.select_tree(node)
		return {'FINISHED'}

class BSI_OBJECT_select_children(Operator):
	"""Select children op"""
	bl_idname = "bsi.object_select_children"
	bl_label = "Select Children"
	bl_options = {'REGISTER', 'UNDO'}

	def modal(self, context, event):
		if event.type in {'LEFTMOUSE', 'WHEELUPMOUSE', 'WHEELDOWNMOUSE'}:
			# allow navigation
			return {'PASS_THROUGH'}
		elif event.type == 'MIDDLEMOUSE':
			node = GUI.pick_object(context, event)
			if node != None:
				OBJ.select_children(node)
			# else:
			# 	bpy.ops.object.select_all(action='DESELECT')
			return {'FINISHED'}
		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			return {'CANCELLED'}

		return {'RUNNING_MODAL'}

	def invoke(self, context, event):
		if context.space_data.type == 'VIEW_3D':
			context.window_manager.modal_handler_add(self)
			return {'RUNNING_MODAL'}
		else:
			self.report({'WARNING'}, "Active space must be a View3d")
			return {'CANCELLED'}


class BSI_OBJECT_nugde(Operator):  
	"""Nudge objects up or down"""  
<<<<<<< HEAD
	bl_idname = "bsi.object_nudge"  
	bl_label = "BSI Nudge Selection"  
=======
	bl_idname = "bsi.object_move3_operator"  
	bl_label = "Nudge Object"  
>>>>>>> origin/master
	bl_options = {'REGISTER', 'UNDO'}  

	direction = FloatVectorProperty(  
									name="vector",  
									default=(0.0, 0.0, 1.0),  
									subtype='XYZ',  
									description="normalized direction vector"  
									)  

	distance = FloatProperty(  
							name="distance",  
							default=0.1,  
							subtype='DISTANCE',  
							unit='LENGTH',  
							description="distance to move with each execution"  
							)  

	def execute(self, context):  
		_dir = self.direction.normalized()  
		context.active_object.location += self.distance * _dir  
		return {'FINISHED'}  

class BSI_OBJECT_reset_xforms(Operator):
	"""BSI Reset transforms"""
	bl_idname = "bsi.object_xform_reset"
	bl_label = "Reset xForms"
	bl_options = {'REGISTER', 'UNDO'}

	scale 		= BoolProperty(name="scale")
	rotation 	= BoolProperty(name="rotation")
	location 	= BoolProperty(name="location")
	
	def execute(self, context):
		sel = bpy.context.selected_objects
		for ob in sel:
			if self.scale:
				ob.scale = mathutils.Vector((1,1,1))
			if self.rotation:
				ob.rotation_euler = mathutils.Vector((0,0,0))
			if self.location:
				ob.location = mathutils.Vector((0,0,0))

		return {'FINISHED'}
	
	@classmethod  
	def poll(cls, context):  
		ob = context.active_object  
		return ob is not None and ob.mode == 'OBJ'  

class BSI_OBJECT_match_xforms(Operator):
	"""BSI match transforms with a ray cast"""
	bl_idname = "bsi.object_xform_match"
	bl_label = "Match xForms"
	
	scale 		= BoolProperty(name="scale")
	rotation 	= BoolProperty(name="rotation")
	location 	= BoolProperty(name="location")

	def modal(self, context, event):
		if event.type in {'MIDDLEMOUSE', 'WHEELUPMOUSE', 'WHEELDOWNMOUSE'}:
			# allow navigation
			return {'PASS_THROUGH'}
		elif event.type == 'LEFTMOUSE':
			dst = utils.pick_object(context, event, ray_max=1000.0)

			if dst != None:
				src = bpy.context.selected_objects
				for ob in src:
					self.match_xfroms(ob, dst)
				return {'FINISHED'} 

			return {'RUNNING_MODAL'}
		elif event.type in {'RIGHTMOUSE', 'ESC', 'SPACE'}:
			return {'CANCELLED'}

		return {'RUNNING_MODAL'}

	def invoke(self, context, event):
		if context.space_data.type == 'VIEW_3D':
			context.window_manager.modal_handler_add(self)
			return {'RUNNING_MODAL'}
		else:
			self.report({'WARNING'}, "Active space must be a View3d")
			return {'CANCELLED'}
			
	def match_xfroms(self, src_ob, dst_ob):
		par = src_ob.parent
		bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
		if self.scale:
			src_ob.scale = dst_ob.scale
		if self.rotation:
			src_ob.rotation_euler = dst_ob.rotation_euler
		if self.location:
			src_ob.location = dst_ob.location
	
		bpy.context.scene.objects.active = par
		bpy.ops.object.parent_set(type='OBJ', keep_transform=True)
		bpy.context.scene.objects.active = src_ob

class BSI_OBJECT_match_xforms(Operator):
	"""BSI match transforms with a ray cast"""
	bl_idname = "bsi.object_xform_match"
	bl_label = "Match xForms"

	scale 		= BoolProperty(name="scale")
	rotation 	= BoolProperty(name="rotation")
	location 	= BoolProperty(name="location")

	def __init__(self):
		self.cursor = OGLW.gl_cursor()

	def modal(self, context, event):
		context.area.tag_redraw()

		if event.type == 'MOUSEMOVE':
			self.mouse_path.append((event.mouse_region_x, event.mouse_region_y))

		elif event.type == 'LEFTMOUSE':
			dst = GUI.pick_object(context, event, ray_max=1000.0)
			if dst != None:
				src = bpy.context.selected_objects
				for ob in src:
					self.match_xfroms(ob, dst)

			bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
			return {'FINISHED'}

		elif event.type in {'RIGHTMOUSE', 'ESC'}:
			bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
			
			GUI.cursor_restore(context)
			self.region.tag_redraw()
			bpy.ops.bsi.wm_redraw_all()
			return {'CANCELLED'}

		return {'RUNNING_MODAL'}

	def invoke(self, context, event):
		if context.area.type == 'VIEW_3D':
			self.mouse_path = [(event.mouse_x,event.mouse_y)]
			self.region = context.region
			self._handle = bpy.types.SpaceView3D.draw_handler_add(
																  self.draw_callback_px, 
																  (context, self.cursor), 
																  'WINDOW', 'POST_PIXEL'
																 )
			GUI.cursor_hide(context)
			self.region.tag_redraw()

			context.window_manager.modal_handler_add(self)
			return {'RUNNING_MODAL'}
		else:
			self.report({'WARNING'}, "View3D not found, cannot run operator")
			return {'CANCELLED'}

	def draw_callback_px(self, context, ogl_cursor):
		font_id = 0  # XXX, need to find out how best to get this.
		blf.position(font_id, 0, -10, 0)
		x = self.mouse_path[-1][0]
		y = self.mouse_path[-1][1]
		bgl.glTranslatef(x, y ,0)
		if ogl_cursor != None:
			bgl.glCallList( ogl_cursor )
		blf.size(font_id, 15, 72)
		blf.draw(font_id, "Pick")

		bgl.glDisable(bgl.GL_BLEND)
		bgl.glColor4f(0.0, 0.0, 0.0, 1.0)

	def match_xfroms(self, src_ob, dst_ob):
		par = src_ob.parent
		bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
		if self.scale:
			src_ob.scale = dst_ob.scale
		if self.rotation:
			src_ob.rotation_euler = dst_ob.rotation_euler
		if self.location:
			src_ob.location = dst_ob.location
	
		bpy.context.scene.objects.active = par
		if par:
			bpy.ops.object.parent_set(type='OBJ', keep_transform=True)
		bpy.context.scene.objects.active = src_ob
