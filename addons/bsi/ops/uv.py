import bpy
import bmesh
import mathutils
from math import degrees, atan, radians, cos, sin
from bpy.types import Operator
# from ..libs import uv_utilities

from collections import defaultdict
from math import cos, sin
from mathutils import Vector, Matrix

class UVData(object):
    def __init__(self, uv_island, uv_center_x, uv_center_y, bb_min, bb_max):
        self.uv_island   = uv_island
        self.uv_center_x = uv_center_x
        self.uv_center_y = uv_center_y
        self.bb_min      = bb_min
        self.bb_max      = bb_max

    def __repr__(self):
        return '{}: {} {} {} {} {}'.format(self.__class__.__name__,
                                  self.uv_island,
                                  self.uv_center_x,
                                  self.uv_center_y,
                                  self.bb_min,
                                  self.bb_max
                                  )

def get_center_x_key(uv_data):
    return uv_data.uv_center_x

def get_center_y_key(uv_data):
    return uv_data.uv_center_y

class MathUtilities(object):
    def __init__(self):
        pass
    def setup_rotation(self, angle, origin=(0, 0)):
        cos_theta, sin_theta = cos(angle), sin(angle)
        x0, y0 = origin
        def xform(point):
            x, y = point[0] - x0, point[1] - y0
            return (x * cos_theta - y * sin_theta + x0,
                    x * sin_theta + y * cos_theta + y0)
        return xform

    def get_2d_vector_average(self, list_of_vectors):
        x, y = 0, 0
        lov = list_of_vectors
        for each in list_of_vectors:
            x += each.x
            y += each.y
        return Vector((x/len(list_of_vectors) ,y/len(list_of_vectors)))

    def get_angle_difference(self, v1, v2):
        r"""Get the angle of the two vetors and then 
           calculate how much rotation is need to get 90 angles"""
        x_diff = v2[0] - v1[0]
        y_diff = v2[1] - v1[1]

        # can't have zeros for division
        if (x_diff == 0.0 or y_diff == 0.0):
            return False
        deg = degrees(atan(y_diff/x_diff))
        rounded_angle = 90 * round(deg/90)
        return deg - rounded_angle

    def view_matricies(self):
        return {
                'TOP':Matrix(((1.0, 0.0, 0.0, 0.0),
                              (0.0, 1.0, 0.0, 0.0),
                              (0.0, 0.0, 1.0, 0.0),
                              (0.0, 0.0, 0.0, 1.0))),
                
                'BOTTOM':Matrix(((0.0, -1.0, 0.0, -1.5865),
                                 (0.0, 0.0, 1.0, -1.0561),
                                 (-1.0, 0.0, 0.0, 9.3510),
                                 (0.0, 0.0, 0.0, 1.0))),
                
                'LEFT':Matrix(((0.0, -1.0, 0.0, -1.5866),
                               (0.0, 0.0, 1.0, -1.0561),
                               (-1.0, 0.0, 0.0, -0.9339),
                               (0.0, 0.0, 0.0, 1.0))),
                
                'RIGHT':Matrix(((1.0, 0.0, 0.0, 0.9340),
                                (0.0, 1.0, 0.0, 1.5867),
                                (0.0, 0.0, 1.0, -1.0),
                                (0.0, 0.0, 0.0, 1.0))),
                
                'FRONT':Matrix(((1.0, 0.0, 0.0, 0.0),
                                (0.0, 0.0, 1.0, 0.0),
                                (0.0, -1.0, 0.0, 0.0),
                                (0.0, 0.0, 0.0, 1.0))),
                
                'BACK':Matrix(((-1.0, 0.0, 0.0, 0.0),
                                (0.0, 0.0, 1.0, 0.0),
                                (0.0, 1.0, 0.0, 0.0),
                                (0.0, 0.0, 0.0, 1.0)))                    
                }

class UvUtilities(object):
    def __init__(self):
        self.face_to_verts = defaultdict(set)
        self.vert_to_faces = defaultdict(set)
        self.selected_faces = {}
        self.selected_island = set()
        self.mesh_obj = None
    
    def refresh_lookup_table(self):
        if hasattr(self.bmesh.faces, "ensure_lookup_table"): 
            self.bmesh.faces.ensure_lookup_table()
   
    def get_uv_data(self, mesh_obj):
        self.mesh_obj = mesh_obj
        self.bmesh = bmesh.from_edit_mesh(self.mesh_obj.data)
        self.uv_layer = self.bmesh.loops.layers.uv.verify()
        self.bmesh.faces.layers.tex.verify()  # currently blender needs both layers.
        self.refresh_lookup_table()

        self.uv_layer = self.bmesh.loops.layers.uv.active
        faces = self.selected_faces['faces']=[]
        uvs = self.selected_faces['uvs']=[]
        
        for face in self.bmesh.faces:
            if face.select:
                faces.append(face)
            for loop in face.loops:
                id = '{0[0]:.5} {0[1]:.5} {1}'.format(loop[self.uv_layer].uv, 
                                                                loop.vert.index)
                self.face_to_verts[face.index].add(id)
                self.vert_to_faces[id].add(face.index)
                if loop[self.uv_layer].select:
                    self.selected_island.add(face.index)
                    if face not in uvs:
                        uvs.append(face)
    
    def set_texture_from_selection(self):
        selected = [f.index for f in self.selected_faces["uvs"]]
        uv_editor = GUI.get_screen_area("IMAGE_EDITOR")
        print (len(selected))
        if len(selected) == 0:
            uv_editor.image = None
            return 
        for poly in  self.mesh_obj.data.polygons:
            if poly.index in selected:
                mat_index = poly.material_index
                mat_slot = None
                if len(self.mesh_obj.material_slots):
                    mat_slot = self.mesh_obj.material_slots[mat_index]
                    mat = mat_slot.material
                    texture = None
                    for tex in mat.texture_slots:
                        if tex is None:
                            continue 
                        if tex.use_map_color_diffuse:
                            texture = tex.texture.image
                            break
                    if len(mat.texture_slots) > 0 and texture is None:
                        texture = mat.texture_slots[0].texture.image
                    uv_editor.image = texture

    def get_island_face_ids(self):
        self.uv_islands = []
        self.faces_left = set(self.face_to_verts.keys())
        while len(self.faces_left) > 0:
            face_id = list(self.faces_left)[0]
            self.current_island = []
            #print(self.faces_left)
            self.add_to_island(face_id)
            self.uv_islands.append(self.current_island)

    def add_to_island(self, face_id):
        if face_id in self.faces_left:
            #add the face itself
            self.current_island.append(face_id)
            #print(face_id)
            self.faces_left.remove(face_id)
            #and add all faces that share uvs with this face
            verts = self.face_to_verts[face_id]
            for vert in verts:
                #print('looking at vert {}'.format(vert))
                connected_faces = self.vert_to_faces[vert]
                if connected_faces:
                    for face in connected_faces:
                        self.add_to_island(face)     
    def get_selection_data(self, bmesh):
        selected_faces = {'faces':[],'uvs':[]}
        faces = selected_faces['faces']
        uvs = selected_faces['uvs']
        uv_layer = bmesh.loops.layers.uv.active
        for zz, face in enumerate(bmesh.faces):
            if face.select:
                faces.append(face)
            for f in face.loops:
                luv = f[uv_layer]
                if luv.select: 
                    if face not in uvs:
                        uvs.append(face)
        return selected_faces

    def uv_bbox(self, island):
        minX = +1000
        minY = +1000
        maxX = -1000
        maxY = -1000
        
        self.refresh_lookup_table()
        
        for face_id in island:
            face = self.bmesh.faces[face_id]
            for loop in face.loops:
                minX = min(loop[self.uv_layer].uv.x, minX)
                minY = min(loop[self.uv_layer].uv.y, minY)
                maxX = max(loop[self.uv_layer].uv.x, maxX)
                maxY = max(loop[self.uv_layer].uv.y, maxY)

        return Vector((minX,minY)), Vector((maxX,maxY))

    def uv_bbox_center(self, island):
        minX = +1000
        minY = +1000
        maxX = -1000
        maxY = -1000

        self.refresh_lookup_table()

        for face_id in island:
            face = self.bmesh.faces[face_id]
            for loop in face.loops:
                minX = min(loop[self.uv_layer].uv.x, minX)
                minY = min(loop[self.uv_layer].uv.y, minY)
                maxX = max(loop[self.uv_layer].uv.x, maxX)
                maxY = max(loop[self.uv_layer].uv.y, maxY)
                
        return  (mathutils.Vector((minX,minY))+ mathutils.Vector((maxX,maxY)))/2

    def island_direction(self, island):
        bbox = self.uv_bbox(island)
        
        width = abs(bbox[0].x - bbox[1].x)
        height = abs(bbox[0].y - bbox[1].y)
        
        print(width, height)
        if width > height:
            return "Horizontal"
        elif width == height:
            return "Same"
        else:
            return "Vertical"

    def add_uv_channel(context):
        obj = context.active_object
        if context.mode != 'EDIT_MESH':
                bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.uv_texture_add()
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.uv.cube_project(scale_to_bounds=True)

    def get_selected_uvs_island(self):
        # Setup an empty dictionary to get filled up with selected
        # {int:[vectors]}
        self.uv_data_dct = {xx: [] for xx, value in enumerate(self.uv_islands)}
        for zz, face in enumerate(self.bmesh.faces):
            for f in face.loops:
                luv = f[self.uv_layer]
                if luv.select:
                    island_id = -1
                    for zz, island in enumerate(self.uv_islands):
                        if f.face.index in island:
                            island_id = zz
                            break
                    if luv.uv not in self.uv_data_dct[island_id]:
                        self.uv_data_dct[island_id].append(luv.uv)

    def rotate_uv_selected(self, angle):
        selected_uvs = []
        vec_2d = Vector((0.0, 0.0))

        for f in self.bmesh.faces:
            for l in f.loops:
                luv = l[self.uv_layer]
                if luv.select:
                    selected_uvs.append(luv)
                    vec_2d += luv.uv

        vec_2d = (vec_2d/len(selected_uvs))
        xfrom_rot = MTH.setup_rotation(radians(angle), vec_2d)
        for luv in selected_uvs:
            luv.uv = xfrom_rot(luv.uv)

    def rotate_uv_island(self, island, angle, position_vector):
        rad = radians(angle)
        xfrom_rot = MTH.setup_rotation(-rad, position_vector)
        for face_id in island:
            face = self.bmesh.faces[face_id]
            for l in face.loops:
                luv = l[self.uv_layer]
                luv.uv = xfrom_rot(luv.uv )

    def rotate_uv_islands_on_selected(self, angle):
        for key in list(self.uv_data_dct):
            if len(self.uv_data_dct[key]) > 0:
                isle = self.uv_islands[key]
                uv_bbox = UV.uv_bbox(isle)
                uv_center = UV.uv_bbox_center(isle)
                self.rotate_uv_island(isle, angle, uv_center)

    def align_island_to_selected(self):
        r""" Aligns the island to verical or horizontal base
            on selected edges or points
        """
        # Process the data structure by getting the angles of the 
        # two vectors first and last elemens from the list

        for key in list(self.uv_data_dct):
            if len(self.uv_data_dct[key]) > 1:
                v1 = (self.uv_data_dct[key][0])
                v2 = (self.uv_data_dct[key][-1])
                angle = MTH.get_angle_difference(v1,v2)

                if not angle:
                    continue
                self.rotate_uv_island(self.uv_islands[key], angle, v1)

    def move_island(self, island, vector):
        for face_id in island:
            for loop in self.bmesh.faces[face_id].loops:
                loop[self.bmesh.loops.layers.uv.active].uv += vector

    def align_uv_islands(self, left=False, top=False, 
                         right=False, bottom=False, horizontal=False, 
                         vertical=False):
        uv_data = []
        self.refresh_lookup_table()
        for key in list(self.uv_data_dct):
            if len(self.uv_data_dct[key]) > 0:
                isle = self.uv_islands[key]
                uv_bbox = UV.uv_bbox(isle)
                center = (uv_bbox[0] + uv_bbox[1])/2
                uv_data.append(UVData(isle, center.x, center.y, 
                                            uv_bbox[0], uv_bbox[1]))

        count = len(uv_data)
        if count > 1:
            for i in range(1, len(uv_data)):
                delta_x = 0.0
                delta_y = 0.0

                if left:
                    delta_x = (uv_data[i-1].bb_min.x - uv_data[i].bb_min.x)
   
                if top:
                    delta_y = (uv_data[i-1].bb_max.y - uv_data[i].bb_max.y)

                if right:
                    delta_x = (uv_data[i-1].bb_max.x - uv_data[i].bb_max.x)

                if bottom:
                    delta_y = (uv_data[i-1].bb_min.y - uv_data[i].bb_min.y)

                if horizontal:
                    curr_center_y = (uv_data[i].bb_min.y + uv_data[i].bb_max.y)/2
                    prev_center_y = (uv_data[i-1].bb_min.y + uv_data[i-1].bb_max.y)/2
                    delta_y = (prev_center_y - curr_center_y)

                if vertical:
                    curr_center_x = (uv_data[i].bb_min.x + uv_data[i].bb_max.x)/2
                    prev_center_x = (uv_data[i-1].bb_min.x + uv_data[i-1].bb_max.x)/2
                    delta_x = (prev_center_x - curr_center_x)

                delta_vec = Vector((delta_x, delta_y))
                self.move_island( uv_data[i].uv_island, delta_vec)
                
                # update the uv_bbox so that we do not recalculate it next loop
                uv_data[i].bb_min.x = uv_data[i].bb_min.x + delta_x
                uv_data[i].bb_max.x = uv_data[i].bb_max.x + delta_x

                uv_data[i].bb_min.y = uv_data[i].bb_min.y + delta_y
                uv_data[i].bb_max.y = uv_data[i].bb_max.y + delta_y

    def distribute_islands(self, axis_x = False, axis_y = False, spacing_value = 0.0):
        r""" Distributes islands of the selected uvs based on given 2d vector"""
        uv_data = []
        self.refresh_lookup_table()
        for key in list(self.uv_data_dct):
            if len(self.uv_data_dct[key]) > 0:
                isle = self.uv_islands[key]
                uv_bbox = UV.uv_bbox(isle)
                center = (uv_bbox[0] + uv_bbox[1])/2
                uv_data.append(UVData(isle, center.x, center.y, 
                                            uv_bbox[0], uv_bbox[1]))

        if axis_x:
            uv_data = sorted(uv_data, key=get_center_x_key)
        if axis_y:
            uv_data = sorted(uv_data, key=get_center_y_key)
            uv_data.reverse()

        count = len(uv_data)
        if count > 1:
             for i in range(1, len(uv_data)):
                delta_x = 0.0
                delta_y = 0.0
                if axis_x:
                    # get delta from left to right
                    delta_x = (uv_data[i-1].bb_max.x + spacing_value) - uv_data[i].bb_min.x
                if axis_y:
                    # Get delat from top to down
                    delta_y = (uv_data[i-1].bb_min.y - spacing_value) - uv_data[i].bb_max.y
                 
                delta_vec = Vector((delta_x, delta_y))
                self.move_island( uv_data[i].uv_island, delta_vec)

                uv_data[i].bb_min.x = uv_data[i].bb_min.x + delta_x
                uv_data[i].bb_max.x = uv_data[i].bb_max.x + delta_x
                uv_data[i].bb_min.y = uv_data[i].bb_min.y + delta_y
                uv_data[i].bb_max.y = uv_data[i].bb_max.y + delta_y

class GuiUtilities(object):
    def __init__(self):
        r""" """
    def get_screen_area(self, type = "VIEW_3D"):
        for area in bpy.context.screen.areas:
            if area.type == type:
                return area.spaces[0] 

    def get_override(self, context):
        for area in context.screen.areas:
            if area.type == 'VIEW_3D':
                for region in area.regions:
                    if region.type == 'WINDOW':
                        return {'area': area, 
                                'region': region,
                                'edit_object': context.edit_object}
        return False

############################### CONSTANTS ###############################
# UV = uv_utilities.UvUtilities()
UV = UvUtilities()
MTH = MathUtilities()
GUI = GuiUtilities()
############################### TANTS ###############################
class BSI_UV_align_islands(Operator):
    """Aligns uv island by islands bounding box"""
    bl_idname  = "bsi.uv_align_bounds"
    bl_label   = "Align Uv Bounds"
    bl_options = {'REGISTER', 'UNDO'}
    
    left       = bpy.props.BoolProperty()
    top        = bpy.props.BoolProperty()
    right      = bpy.props.BoolProperty()
    bottom     = bpy.props.BoolProperty()
    horizontal = bpy.props.BoolProperty()
    vertical   = bpy.props.BoolProperty()

    @classmethod
    def poll(cls, context):
        return (context.mode == 'EDIT_MESH')

    def execute(self, context):
        obj = context.active_object
        UV.get_uv_data(obj)

        UV.get_island_face_ids()
        UV.get_selected_uvs_island()
        if len(UV.selected_island) > 1:
            l,t,r,b,h,v, = False, False, False, False, False, False
            l = self.left
            t = self.top
            r = self.right
            b = self.bottom
            h = self.horizontal
            v = self.vertical
            UV.align_uv_islands( l, t, r, b, h, v)
        obj.data.update()
        return {'FINISHED'}
      
class BSI_UV_sandbox(Operator):
    """Aligns Island 90 degrees using selected edge"""
    bl_idname = "bsi.uv_sandbox"
    bl_label = "Aligns Island to 90 degrees using selected edge"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        # print(context.mode)
        obj = context.active_object

        # Gather uv data
        UV.get_uv_data(obj)

        # ############ Rotate Selection ###################
        # UV.rotate_uv_selected(90)
        ###################################################

        UV.get_island_face_ids()
        UV.get_selected_uvs_island()

        #addUvChannel(context)
        obj.data.update()
        return {'FINISHED'}

class BSI_UV_distribute_islands(Operator):
    """Distributes uv islands based on sub selection"""
    bl_idname  = "bsi.uv_distribute_islands"
    bl_label   = "Distribute Islands"
    bl_options = {'REGISTER', 'UNDO'}

    x = bpy.props.BoolProperty(default=False)
    y = bpy.props.BoolProperty(default=False)
    spacing = bpy.props.FloatProperty(default=0.05)

    @classmethod
    def poll(cls, context):
        return (context.mode == 'EDIT_MESH')

    def execute(self, context):
        obj = context.active_object
        # Gather uv data
        UV.get_uv_data(obj)
        UV.get_island_face_ids()
        UV.get_selected_uvs_island()

        UV.distribute_islands(self.x, self.y, self.spacing)

        obj.data.update()
        return {'FINISHED'}

class BSI_UV_rotate_90(Operator):
    """Rotate uv islands based on edge sub-selection"""
    bl_idname  = "bsi.align_island_to_selected"
    bl_label   = "Align Islands 90"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return (context.mode == 'EDIT_MESH')

    def execute(self, context):
        obj = context.active_object
        # Gather uv data
        UV.get_uv_data(obj)
        UV.get_island_face_ids()
        UV.get_selected_uvs_island()
        UV.align_island_to_selected()
        obj.data.update()
        return {'FINISHED'}

class BSI_UV_project_planar(Operator):
    """Project planar of selected uvs on """
    bl_idname = "bsi.uv_project_planar"
    bl_label = "Project Planar"
    bl_options = {'REGISTER', 'UNDO'}

    direction  = bpy.props.StringProperty(default='RIGHT')

    def execute(self, context):
        obj = context.active_object
        bm = bmesh.from_edit_mesh(obj.data)
        UV.get_uv_data(obj)


        dir_dct = MTH.view_matricies()
        if self.direction in list(dir_dct):
            bpy.context.area.type = 'VIEW_3D'
            v3d = GUI.get_screen_area().region_3d
            view_matrix = Matrix((v3d.view_matrix))
            view_type   = v3d.view_perspective
            v3d.view_perspective = 'ORTHO'
            v3d.view_matrix = dir_dct[self.direction]
            for face in bm.faces:
                if face in UV.selected_faces['uvs']:
                    continue
                face.select = False
            v3d.update()
            override = GUI.get_override(context)
            if override:
                bpy.ops.uv.project_from_view(override, 
                                             orthographic=True, 
                                             correct_aspect=False, 
                                             clip_to_bounds=True, 
                                             scale_to_bounds=False)
            for face in bm.faces:
                if face in UV.selected_faces['faces']:
                    face.select = True
                
            v3d.view_perspective = view_type
            v3d.view_matrix = view_matrix
            v3d.update()
            bpy.context.area.type = 'IMAGE_EDITOR'

        return {'FINISHED'}

class BSI_UV_texture_from_selection(Operator):
    """Set texture from uv sub selection"""
    bl_idname = "bsi.uv_texture_from_selection"
    bl_label = "Set Uv Texture"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        obj = context.active_object
        UV.get_uv_data(obj)
        UV.set_texture_from_selection()
        return {'FINISHED'}

def register():
    bpy.utils.register_class(BSI_UV_sandbox)
    bpy.utils.register_class(BSI_UV_align_islands)
    bpy.utils.register_class(BSI_UV_distribute_islands)
    bpy.utils.register_class(BSI_UV_rotate_90)
    bpy.utils.register_class(BSI_UV_project_planar)
    bpy.utils.register_class(BSI_UV_texture_from_selection)


def unregister():
    bpy.utils.unregister_class(BSI_UV_sandbox)
    bpy.utils.unregister_class(BSI_UV_align_islands)
    bpy.utils.unregister_class(BSI_UV_distribute_islands)
    bpy.utils.unregister_class(BSI_UV_rotate_90)
    bpy.utils.unregister_class(BSI_UV_project_planar)
    bpy.utils.unregister_class(BSI_UV_texture_from_selection)


if __name__ == "__main__":
    register()

    # test call
    # bpy.ops.bsi.uv_sandbox()



